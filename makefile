# All Targets
all: myasm

# Tool invocations
# Executable "ass3" depends on the files
myasm: myasm.o
	gcc -g -m64 -o foo myasm.o

# Depends on the source and header files
#scheme.o: scheme.s
#	nasm -f elf64 -l scheme.lst scheme.s

myasm.o: myasm.s
	nasm -g -f elf64 -l myasm.lst myasm.s

myasm.s:
	scheme --script compiler.scm foo.scm myasm.s

#tell make that "clean" is not a file name!
.PHONY: clean

#Clean the build directory
clean:
	rm -f *.o foo *.lst foo.s myasm.s

