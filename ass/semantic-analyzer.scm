(define remove-applic-lambda-nil
	(lambda (texp)
		(if (list? texp)
      (if (and (= (length  texp) 3) (equal? 'applic (car texp))  (lambda? (cadr texp)) (equal? (caddr texp) '()))
        (let* ((function (cadr texp)) (func-type (car function)) (params (get-params function)) (body (get-body function)))
          (if (and (lambda? function) (equal? params '()))
            (remove-applic-lambda-nil body)
            (map remove-applic-lambda-nil texp)))
        (map remove-applic-lambda-nil texp))    
      texp)
  ))

(define var?
  (lambda (exp)
    (if (and (list? exp) (equal? (length exp) 2) (equal? (car exp) 'var))
      #t
      #f)))

(define is-member?
  (lambda (el list)
    (if (member el list)
      #t
      #f )))


(define lambda?
  (lambda (exp)
    (if (and (list? exp) (not (empty? exp)) (or (equal? (car exp) 'lambda-simple) (equal? (car exp) 'lambda-opt)))
      #t
      #f)
    ))

(define pe->lex-pe
  (lambda (texp)
    (if (lambda? texp)
      (if (equal? (car texp) 'lambda-simple)
        (list (car texp) (cadr texp) (aux-lex-pe (get-body texp) '() (get-params texp)))
        (list (car texp) (cadr texp) (caddr texp) (aux-lex-pe (get-body texp) '() (get-params texp)))
        )
      (aux-lex-pe texp '() '()))
 
  ))

(define aux-lex-pe
  (lambda (texp env prev-params)
    (cond ((var? texp) (find-in-env-or-params (cadr texp) env prev-params))
          ((lambda? texp) (let ((cur-params (get-params texp)) (body (caddr texp)) (new-env (extend-env env prev-params)))
            (map (lambda (el) (aux-lex-pe el new-env cur-params)) texp)))
          ((list? texp) (map (lambda (el) (aux-lex-pe el env prev-params)) texp))
          (else texp))
    ))

(define extend-env
  (lambda (old-env params)
    (append (list params) old-env)
  ))

(define empty?
  (lambda (lst)
    (if (and (list? lst) (equal? lst '()))
      #t
      #f)))


(define index-of
  (lambda (var-name lst index)
    (cond ((empty? lst) -1)
          ((equal? (car lst) var-name) index)
          (else (index-of var-name (cdr lst) (+ 1 index))))
    ))

(define find-in-params
  (lambda (var-name params)
    (if (empty? params)
      (list 'fvar var-name)
      (let ((index (index-of var-name params 0)))
        (if (equal? index -1)
          (list 'fvar var-name)
          (list 'pvar var-name index)))
      )))



(define find-in-env-or-params
  (lambda (var-name env params) 
    (letrec 
      ((is-bound? (lambda (var-name env major)
          (if (empty? env)
            (find-in-params var-name params)
            (let ((index (index-of var-name (car env) 0)))
              (if (equal? -1 index)
                (is-bound? var-name (cdr env) (+ 1 major))
                (list 'bvar var-name major (index-of var-name (car env) 0))
                ))))))
    

      (if (is-member? var-name params)
        (find-in-params var-name params)
        (is-bound? var-name env 0)
        )
    )
  ))

(define remove-dups
  (lambda (lst)
    (if (empty? lst)
      lst
      (if (is-member? (car lst) (cdr lst))
        (remove-dups (cdr lst))
        (append (list (car lst)) (remove-dups (cdr lst))))

      )
    ))

(define get-bound-vars
  (lambda (texp acc)
    (if (or (not (list? texp)) (empty? texp))  
      acc
      (if (and (equal? (car texp) 'bvar) (not (is-member? (cadr texp) acc)))
        (append acc (list (cadr texp)))
        (let ((bound-in-car (get-bound-vars (car texp) acc)) 
          (bound-in-cdr (get-bound-vars (cdr texp) acc)))
          (if (equal? bound-in-car bound-in-cdr)
            (append acc bound-in-car)
            (append acc bound-in-car bound-in-cdr)


          )
        )
        ))
    ))

(define in-set?
  (lambda (tagged-exp var)
    (if (or (not (list? tagged-exp)) (empty? tagged-exp))
      #f
      (if (and (equal? (car tagged-exp) 'set) (and (or (equal?(caadr tagged-exp) 'pvar)(equal?(caadr tagged-exp) 'bvar)) (equal? (cadadr tagged-exp) var)) )
        #t
        (if (lambda? tagged-exp)
          (let ((params (get-params tagged-exp)))
            (if (is-member? var params)
              #f
              (or (in-set? (car tagged-exp) var) (in-set? (cdr tagged-exp) var)))
            )
          (or (in-set? (car tagged-exp) var) (in-set? (cdr tagged-exp) var))
          )
        )
      )
    ))

(define has-get-occurrence?
  (lambda (texp var)
    (if (or (not (list? texp)) (empty? texp))  
      #f
      (if (equal? (car texp) 'set)  
        (has-get-occurrence? (caddr texp) var)
        (if (and (or (equal? (car texp) 'pvar)(equal? (car texp) 'bvar)) (equal? (cadr texp) var))
          #t
          (if (lambda? texp)
            (let ((params (get-params texp)))
              (if (is-member? var params)
                #f              
                (or (has-get-occurrence? (car texp) var)(has-get-occurrence? (cdr texp) var)))
                  )
            (or (has-get-occurrence? (car texp) var) (has-get-occurrence? (cdr texp) var))))))
    ))

(define get-params
  (lambda (lambda-exp)
    (if (equal? (car lambda-exp) 'lambda-simple)
      (cadr lambda-exp)
      (append (cadr lambda-exp) (list (caddr lambda-exp)))
      )
    ))

(define is-bound?
  (lambda (tagged-exp var)
    (if (or (not (list? tagged-exp)) (empty? tagged-exp))
      #f
      (if (and (equal? (car tagged-exp) 'bvar) (equal? (cadr tagged-exp) var))
        #t
        (if (lambda? tagged-exp)
          (let ((params (get-params tagged-exp)))
            (if (is-member? var params)
              #f
              (or (is-bound? (car tagged-exp) var) (is-bound? (cdr tagged-exp) var)))
            )
          (or (is-bound? (car tagged-exp) var) (is-bound? (cdr tagged-exp) var))
          )
        )
      )

    ))

(define get-body
  (lambda (lambda-exp)
    (if (equal? (car lambda-exp) 'lambda-simple)
      (caddr lambda-exp) 
      (cadddr lambda-exp)
      )

    ))

(define vars-to-box
  (lambda (params res-lst acc)
    (if (empty? res-lst)
      acc
      (if (equal? #t (car res-lst))
        (vars-to-box (cdr params) (cdr res-lst) (append acc (list (car params))))
        (vars-to-box (cdr params) (cdr res-lst) acc)
        )
      )
    ))

(define substract-lists
  (lambda (params new-params acc)
    (if (empty? params)
      acc
      (if (is-member? (car params) new-params)
        (substract-lists (cdr params) new-params acc)
        (substract-lists (cdr params) new-params (append acc (list (car params))))
        )
      )
    ))

(define box-criteria2-3
  (lambda (texp params)
    (if (and (list? texp) (not (empty? texp)))
      (if (and (equal? 'set (car texp)) (list? (caddr texp)) (equal? (caaddr texp) 'box))
        texp
        (cond ((equal? 'set (car texp))
              (let ((var-to-set (cadadr texp)))
                (if (is-member? var-to-set params)
                  (list 'box-set (cadr texp) (box-criteria2-3 (caddr texp) params))
                  (list 'set (cadr texp) (box-criteria2-3 (caddr texp) params)))

              ))
            ((equal? 'var (car texp))
              (if (is-member? (cadr texp) params)
                (list 'box-get texp)
                texp))
            ((lambda? texp) (let ((lambda-type (car texp)) (new-params (substract-lists params (get-params texp) '())) (body (get-body texp)))
              (if (equal? lambda-type 'lambda-opt)
                (list lambda-type (cadr texp) (caddr texp) (box-criteria2-3 body new-params))
                (list lambda-type (get-params texp) (box-criteria2-3 body new-params)))
              ))
            (else (append (list (box-criteria2-3 (car texp) params)) (box-criteria2-3 (cdr texp) params)))
          ))
      texp
      )
    ))


(define generate-box-statement
  (lambda (lst)
    (map (lambda (var) (list 'set (list 'var var) (list 'box (list 'var var)))) lst)
    ))

(define do-boxing
  (lambda (texp vars-for-boxing)
    (if (empty? vars-for-boxing)
      texp
      (cond ((equal? (car texp) 'lambda-simple) 
              (let ((lambda-type (car texp)) (params (get-params texp)) (body (caddr texp)))
                (if (equal? (car body) 'seq)
                  (list lambda-type params (list 'seq (append (generate-box-statement vars-for-boxing) (box-criteria2-3 (cadr body) vars-for-boxing))))
                  (list lambda-type params (list 'seq (append (generate-box-statement vars-for-boxing) (list (box-criteria2-3 body vars-for-boxing)))))
                  )

                ))
            ((equal? (car texp) 'lambda-opt)
              (let* ((lambda-type (car texp)) (params-a (cadr texp)) (params-b (caddr texp)) (params-ab (append params-a (list params-b))) (body (cadddr texp)))
                (if (equal? (car body) 'seq)
                  (list lambda-type params-a params-b (list 'seq (append (generate-box-statement vars-for-boxing) (box-criteria2-3 (cadr body) vars-for-boxing))))
                  (list lambda-type params-a params-b (list 'seq (append (generate-box-statement vars-for-boxing) (list (box-criteria2-3 body vars-for-boxing)))))
                  ) 
                ) )
        )
      )
    ))


(define box-set
  (lambda (texp)
    (begin 
      (if (or (not (list? texp)) (empty? texp))
          texp
          (if (lambda? texp)
            (let* ((params (get-params texp)) (cur-lambda texp) (tagged-cur-lambda (pe->lex-pe texp))
              (res-lst (map (lambda (var)(and (is-bound? (get-body tagged-cur-lambda) var)(in-set? (get-body tagged-cur-lambda) var)(has-get-occurrence? (get-body tagged-cur-lambda) var))) params))
              (vars-for-boxing (vars-to-box params res-lst '())) (res (do-boxing texp vars-for-boxing)))
              (begin 
                (if (equal? (car texp) 'lambda-simple)
                  (list (car res) (cadr res)(box-set (get-body res)))
                  (list (car res) (cadr res) (caddr res) (box-set(get-body res)))
                  )
    
                )
              )
            (begin 
              (append (list (box-set (car texp))) (box-set (cdr texp))))
            )
          ))
    ))


(define constTexp?
  (lambda(exp) 
    (if (equal?(car exp) 'const) #t #f)
  ))
  
(define define?
  (lambda(exp) 
    (if (equal?(car exp) 'define) #t #f)
  ))
  
(define if?
  (lambda(exp) 
    (if (equal?(car exp) 'if3) #t #f) 
  ))

(define applic?
  (lambda(exp) 
    (if (equal?(car exp) 'applic) #t #f) 
  ))
(define or?
  (lambda(exp) 
    (if (equal?(car exp) 'or) #t #f) 
  ))

(define seq?
  (lambda(exp) 
    (if (equal?(car exp) 'seq) #t #f) 
  ))
(define set?
  (lambda(exp) 
    (if (or (equal?(car exp) 'box-set) (equal?(car exp) 'set)) #t #f) 
  ))

(define annotate-tc
  (lambda(pe)
  (annotate-tc-rec pe #f)))
  
(define annotate-tc-rec
  (lambda(pe tp?)
  (cond 
    ((constTexp? pe) pe)
    ((var? pe) pe)
    ((lambda? pe) 
    (if (equal? 'lambda-simple (car pe))
    (list (car pe) (cadr pe) (annotate-tc-rec (caddr pe) #t)) 
    (list (car pe) (cadr pe) (caddr pe) (annotate-tc-rec (cadddr pe) #t))))
    ((define? pe) (list (car pe) (cadr pe) (annotate-tc-rec (caddr pe) #f)))
    ((if? pe) (list (car pe) (annotate-tc-rec (cadr pe) #f)(annotate-tc-rec (caddr pe) tp?) (annotate-tc-rec (cadddr pe) tp?)))
  ((or? pe) (let* ((body (cadr pe))(rev-body (reverse body)) (last-element (car rev-body)))
    
  
  (list (car pe) (append (map (lambda(el) (annotate-tc-rec el #f))(reverse(cdr rev-body))) (list(annotate-tc-rec last-element tp?))))))
  
  
    ((applic? pe) (if (equal? tp? #t)(list 'tc-applic (annotate-tc-rec (cadr pe) #f) (map (lambda(el)(annotate-tc-rec el #f)) (caddr pe)))
      (list 'applic (annotate-tc-rec (cadr pe) #f) (map (lambda(el)(annotate-tc-rec el #f)) (caddr pe)))))
  ((seq? pe) (let* ((body (cadr pe))(rev-body (reverse body))(last-element (car rev-body)))
    (list (car pe) (append (map (lambda(el) (annotate-tc-rec el #f))(reverse(cdr rev-body))) (list (annotate-tc-rec last-element tp?))))))
  ((set? pe) (list (car pe) (cadr pe) (annotate-tc-rec (caddr pe) #f)))
  
  (else pe)
         
  )))


