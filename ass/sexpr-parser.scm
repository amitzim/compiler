(load "project/ass/pc.scm")

(define string-append-list
	(lambda (l)
		(apply string-append l)))
		
(define <InlineComments2>
  (new
   (*parser (word ";"))

   (*parser (diff (delayed (lambda () <Whitespace>)) (word "\n")))
   (*parser (delayed (lambda () <VisibleSimpleChar>)))
   (*disj 2) *star

   (*parser (word "\n"))
   (*parser <end-of-input>)
   (*disj 2)

   (*caten 3)
   (*pack-with (lambda x 'comment))
  done))

(define <Comment>
    (new
		(*parser <InlineComments2>)
		(*parser (delayed (lambda () <ExpressionComments>)))
		(*parser (delayed (lambda () <Whitespace>)))
		(*disj 3)

        (*pack (lambda x 'comment))
        done))
  
(define <sexpr>
	(new
		(*parser <Comment>) *star

		(*parser (delayed (lambda () <Vector>)))
		(*parser (delayed (lambda () <Boolean>)))
		(*parser (delayed (lambda () <Number>)))
		(*parser (delayed (lambda () <Symbol>)))
		(*parser (delayed (lambda () <Char>)))
		(*parser (delayed (lambda () <ProperList>)))
		(*parser (delayed (lambda () <ImproperList>)))
		(*parser (delayed (lambda () <UnquoteAndSpliced>)))
		(*parser (delayed (lambda () <Unquoted>)))
		(*parser (delayed (lambda () <Quoted>)))
		(*parser (delayed (lambda () <QuasiQuoted>)))
		(*parser (delayed (lambda () <String>)))	
		(*parser (delayed (lambda () <CBName>)))
		(*parser (delayed (lambda () <InfixExtension>)))
		(*disj 14)
	
		(*parser <Comment>) *star
		(*caten 3)
		(*pack-with (lambda (ws1 sexpr ws2) sexpr))
	done)
	)	


(define <ExpressionComments>
	(new
		(*parser (word "#;"))
		(*parser <sexpr>)
		(*caten 2)
	done))



(define <Whitespace>
	(new
		(*parser (const (lambda (ch) (char<=? ch #\space))))
	done))

(define <Left-Parentheses>
	(new
		(*parser (word "("))
		(*pack (lambda (_) (integer->char 40) ))
	done))

(define <Right-Parentheses>
	(new
		(*parser (word ")"))
		(*pack (lambda (_) (integer->char 41)))
	done))


; Boolean Parser
(define <Boolean>
	(new
		(*parser <Whitespace>) *star
		(*parser (word-ci "#f"))
		(*pack (lambda (_) #f))
		(*parser (word-ci "#t"))
		(*pack (lambda (_) #t))
		(*disj 2)
		(*caten 2)
		(*pack-with (lambda (ws1 bool) bool))

		done))



(define <CharPrefix>
	(new
		(*parser (word "#\\"))
		(*pack (lambda (_) "#\\"))
	done))

(define <VisibleSimpleChar>
	(new
		(*parser (const (lambda (ch) (char<=? #\! ch))))
	done))



(define <NamedChar>
	(new
		(*parser (word-ci "lambda"))
		(*pack (lambda (_) (integer->char 955)))
		(*parser (word-ci "newline"))
		(*pack (lambda (_) #\newline))
		(*parser (word-ci "nul"))
		(*pack (lambda (_) #\nul))
		(*parser (word-ci "page"))
		(*pack (lambda (_) #\page))
		(*parser (word-ci "return"))
		(*pack (lambda (_) #\return))
		(*parser (word-ci "space"))
		(*pack (lambda (_) #\space))
		(*parser (word-ci "tab"))
		(*pack (lambda (_) #\tab))
		(*disj 7)
	done))


(define <HexChar>
	(new
		(*parser (range-ci #\a #\f))
		(*parser (range #\0 #\9))
		(*disj 2)
	done))

(define <HexUnicodeChar>
	(new
		(*parser (word-ci "x"))
		(*parser <HexChar>) *plus
		(*caten 2)
		(*pack-with (lambda (ex ans)
			(integer->char (string->number(list->string ans) 16))
			))

	done))


(define <Char>
	(new
		(*parser <CharPrefix>)
		(*parser <NamedChar>)
		(*parser <HexUnicodeChar>)
		(*parser <VisibleSimpleChar>)
		(*disj 3)
		(*caten 2)
		(*parser <Whitespace>)
		(*parser <end-of-input>)
		(*parser <Right-Parentheses>)
		(*disj 3)
		*followed-by
		(*pack (lambda (ans) (cadr ans)))
	done))

(define <Natural>
	(new
		(*parser (plus (range #\0 #\9)))
		(*pack (lambda (num-in-list) (string->number(list->string  num-in-list) 10)))
	done))


(define <Integer>
	(new
		(*parser (word "+"))
		(*parser (word "-"))
		(*disj 2)
		*maybe
		(*parser <Natural>)
		(*caten 2)
		(*pack-with (lambda (sign-list number)
			(if (equal?(cadr sign-list) #f)
				number
				(let ((sign? (car sign-list)) (the-sign (caadr sign-list)))
					(if (not sign?)
						number
						(if (equal? the-sign #\+)
							number
							(- 0 number)))))))

	done))

(define <Fraction>
	(new
		(*parser <Whitespace>) *star
		(*parser <Integer>)
		(*parser (word "/"))
		(*parser <Natural>)
		(*caten 3)
		(*pack-with (lambda (mone slash-sign mehane) (/ mone mehane)))
		(*parser <Whitespace>) *star
		(*caten 3)
		(*pack-with (lambda (ws1 frac ws2) frac))
	done))

(define <Number>
	(new
		(*parser <Whitespace>) *star
		(*parser <Fraction>)
		(*parser <Integer>)
		(*disj 2)
		(*caten 2)

		(*parser (range #\a #\z))
		(*parser (range #\A #\Z))
		(*disj 2)
		*not-followed-by
		(*pack-with (lambda (ws1 ans) ans))
	done))

(define <StringHexChar>
	(new
		(*parser (word-ci "\\x"))
		(*parser <HexChar>) *star
		(*parser (word ";"))
		(*caten 3)
		(*pack-with (lambda(backslash-x number smi-colon) 
		
			(string (integer->char(string->number(list->string number) 16)))))
		done))


;; returns string
(define <StringMetaChar>
	(new
		(*parser (word "\\\\"))
		(*pack (lambda (_) "\\"))
		(*parser (word "\\\""))
		(*pack (lambda (_) "\""))
		(*parser (word "\\\t"))
		(*pack (lambda (_) "\t"))
		(*parser (word "\\\f"))
		(*pack (lambda (_) "\f"))
		(*parser (word "\\\n"))
		(*pack (lambda (_) "\n"))
		(*parser (word "\\\r"))
		(*pack (lambda (_) "\r"))
		(*disj 6)
		(*pack (lambda (ans) ans))
	done))

(define <StringLiteralChar>
	(new
		(*parser (const (lambda (ch) (not(or (char=? ch #\\) (char=? ch #\"))))))
		(*pack (lambda(ch) (string ch)))
		done))

(define <StringChar>
	(new
		(*parser <StringMetaChar>)
		(*parser <StringHexChar>)
		(*parser <StringLiteralChar>)
		(*disj 3)
		
	done))

(define <String>
	(new
		
		(*parser (word "\"")) 
		(*parser <StringChar>) *star
		(*parser (word "\"")) 
		
		(*caten 3)
		(*pack-with (lambda (lpar ans rpar) (string-append-list ans)))
		
	done))



(define <SymbolChar>
	(new
		(*parser (range #\0 #\9))
		; (*pack-with (lambda (ans) ans))
		(*parser (range-ci #\a #\z))
		; (*pack-with (lambda (ans) ans))
		(*parser (word "!"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "$"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "^"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "*"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "-"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "_"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "="))
		(*pack-with (lambda (ans) ans))
		(*parser (word "+"))
		(*pack-with (lambda (ans) ans))
		(*parser (word ">"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "<"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "?"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "/"))
		(*pack-with (lambda (ans) ans))
		(*disj 14)
	done))

(define <Symbol>
	(new
		(*parser <Whitespace>) *star
		(*parser (plus <SymbolChar>))
		(*caten 2)
		(*pack-with (lambda (ws1 ans) (string->symbol (string-downcase (list->string ans)))))
	done))



(define <ProperList>
	(new
		(*parser <Whitespace>) *star
		(*parser <Left-Parentheses>)
		(*parser <Whitespace>) *star
		(*parser <sexpr>)
		(*parser <Whitespace>) *star
		(*caten 2) *star
		(*parser <Right-Parentheses>)
		(*parser <Whitespace>) *star
		(*caten 6)
		(*pack-with (lambda (ws1 lpar par2 par3 rpar ws2) (map car par3)))
	done))


(define <ImproperList>
  	(new
  		(*parser <Whitespace>) *star
  		(*parser <Left-Parentheses>)
  		(*parser <Whitespace>) *star
  		

  		;;;; number ;;;;;
  		(*parser <Number>)
  		(*parser <sexpr>)
  		(*disj 2)
  		(*parser <Whitespace>) *star
  		(*caten 2)*plus
                

  		;;;; not a number ;;;;;
  		(*parser <sexpr>)
  		(*parser <Whitespace>) *star
  		(*caten 2)*plus 
  		
  		(*disj 2)


  		(*pack (lambda (ans) (map car ans)))
  		(*parser <Whitespace>) *star
  		(*parser (word "."))
  		
  		;;;;;  case 2 ;;;;;;;;;;;
  		(*parser <Whitespace>) *plus
  		(*parser <Number>)
  		(*caten 2)
  		(*pack-with (lambda (ws number) number))

  		
  		;;;;;  case 1 ;;;;;;;;;;;

  		(*parser <Whitespace>) *star
  		(*parser <sexpr>)
  		(*caten 2)
  		(*pack-with (lambda (ws sexpr) sexpr))
  		(*disj 2)

  		(*parser <Whitespace>) *star
  		(*parser <Right-Parentheses>)
  		(*parser <Whitespace>) *star
  		(*caten 10)
  		(*pack-with (lambda (ws1 lpar ws2 sexpr1 ws3 dot sexpr2 ws4 rpar ws5)  			
  			(list->pair sexpr1 sexpr2)
  			))
  	done))


(define list->pair
	(lambda (lst el)
		(if (equal?  lst '())
			el
			(cons (car lst) (list->pair (cdr lst) el)))
		))



(define <Quoted>
	(new
		(*parser (word "'"))
		(*parser <Whitespace>) *star
		(*parser <sexpr>)
		(*caten 3)
		(*pack-with (lambda (quote-mark ws expr) 
			(list 'quote expr)
			))
	done))


(define <Unquoted>
	(new
		(*parser (word ","))
		(*parser <Whitespace>) *star
		(*parser <sexpr>)
		(*caten 3)
		(*pack-with (lambda (comma ws sexpr)
			(list 'unquote sexpr)))
	done))



(define <UnquoteAndSpliced>
	(new
		(*parser (word ",@"))
		(*parser <Whitespace>) *star
		(*parser <sexpr>)
		(*caten 3)
		(*pack-with (lambda (comma-at ws sexpr)
			(list 'unquote-splicing sexpr)))
	done))


(define <Vector>
	(new
			(*parser (word "#"))
			(*parser <Left-Parentheses>)
			(*parser <Whitespace>)*star
			(*parser <sexpr>)
			(*parser <Whitespace>)*star
			(*caten 2)*star
			(*parser <Right-Parentheses>)
			(*caten 5)
			(*pack-with(lambda(a b c d e) 
				(let ((lst (map car d)))
					(list->vector lst))))
	done))



(define <QuasiQuoted>
	(new
		(*parser (word "`")) ;1
		(*parser <Whitespace>) *star ;2
		(*parser <sexpr>)
		(*caten 3)
		(*pack-with (lambda (qq ws sexpr)
			(list 'quasiquote sexpr)
			))
	done))
			

(define <CBNameSyntax1>
	(new
		(*parser (word "@"))
		(*parser <sexpr>)
		(*caten 2)
		(*pack (lambda (ans) (list 'cbname (cadr ans))))
	done))

(define <CBNameSyntax2>
	(new
		(*parser (word "{"))
		(*parser <Whitespace>) *star
		(*parser <sexpr>)
		(*parser <Whitespace>) *star
		(*parser (word "}"))		
		(*caten 5)
		(*pack (lambda (ans) (list 'cbname (caddr ans))))
	done))

(define <CBName>
	(new
		(*parser <CBNameSyntax1>)
		(*parser <CBNameSyntax2>)
		(*disj 2)
	done))


(define <InfixExpression>
	(new
		(*parser (delayed (lambda () <InfixFuncall>)))
		(*parser (delayed (lambda () <InfixSubstractionAndAddition>)))
		(*parser (delayed (lambda () <InfixDivisionAndMultiplication>)))
		(*parser (delayed (lambda () <InfixPower>)))
		(*parser (delayed (lambda () <InfixArrayGet>)))
		(*parser (delayed (lambda () <HighestPriority>)))
		(*parser (delayed (lambda () <InfixSymbol>))) 
		(*disj 7)
	done))

(define <InfixPrefixExtensionPrefix>
	(new
		(*parser (word "##"))
		(*parser (word "#%"))
		(*disj 2)
	done))
	
(define <PowerSymbol>
	(new
		(*parser (word "^"))
		(*parser (word "**"))
		(*disj 2)
			
	done))



(define <SymbolForInfix>
	(new
		(*parser (range #\0 #\9))
		
		(*parser (range-ci #\a #\z))
		
		(*parser (word "!"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "$"))
		(*pack-with (lambda (ans) ans))		
		(*parser (word "_"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "="))
		(*pack-with (lambda (ans) ans))
		(*parser (word ">"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "<"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "?"))
		(*pack-with (lambda (ans) ans))
		(*parser (word "/"))
		(*pack-with (lambda (ans) ans))
		(*disj 10)
	done))

(define <InfixSymbol>
	(new
		(*parser <Whitespace>) *star
		(*parser (plus <SymbolForInfix>))
		(*caten 2)
		(*pack-with (lambda (ws1 ans) (string->symbol (string-downcase (list->string ans)))))
	done))




(define equation-builder
	(lambda (lst op)
		(if (equal? (length lst) 2) 
			(list op (cadr lst) (car lst))
			(list op (equation-builder (cdr lst) op) (car lst))
		)))



(define <InfixNeg>
	(new
		(*parser (word "-"))
		(*parser <Whitespace>) *star
		(*parser <InfixExpression>)
		(*caten 3)
		(*pack-with (lambda (minus ws1 number) 
			(list '- number)))
	done))

(define <InfixPar>
	(new
		(*parser <Whitespace>) *star
		(*parser <Left-Parentheses>)
		(*parser <Whitespace>) *star
		(*parser <InfixExpression>)
		(*parser <Whitespace>) *star
		(*parser <Right-Parentheses>)
		(*parser <Whitespace>) *star
		(*caten 7)
		(*pack-with (lambda (ws1 lpar ws2 expr ws3 rpar ws4) expr))
		
	done))

(define <HighestPriority>
	(new
		(*parser <Number>)
		(*parser <InfixPar>)
		(*parser <InfixNeg>)
		(*parser (delayed (lambda () <InfixSexprEscape>)))	
		
		(*parser <InfixSymbol>)
		(*parser <InfixExpression>)
		(*disj 6)
	done))


(define vector-builder
	(lambda (name lst-nums)
		(if (equal? (length lst-nums) 1)
			(list 'vector-ref name (car lst-nums))
			(list 'vector-ref (vector-builder name (cdr lst-nums)) (car lst-nums))
			)))

(define <InfixArrayGet>
	(new
		(*parser <Whitespace>) *star
		(*parser <HighestPriority>)
	
		(*parser <Whitespace>) *star
		(*caten 3)
		(*pack-with (lambda (ws1 expr ws2) expr))
		(*parser (word "[")) 
		(*parser <Whitespace>) *star
		(*parser <InfixExpression>) 
		(*parser <Whitespace>) *star
		(*parser (word "]"))
		(*caten 5) *star
		(*pack (lambda (ans) (map caddr ans)))
		(*caten 2)
		(*pack-with (lambda (vector-name indexes)
			(if (equal? indexes '())
				vector-name
				(vector-builder vector-name (reverse indexes))
				)
			))
	done))

(define extract-numbers
	(lambda (lst)
		(cadr(caaddr lst))))

(define build-pow
	(lambda (lst)
		(if (equal? (length lst) 2)
			(list 'expt (car lst) (cadr lst))
			(list 'expt  (car lst) (build-pow (cdr lst))))))

(define <InfixPower>
	(new
		(*parser <Whitespace>) *star
		(*parser (delayed (lambda () <InfixFuncall>)))
		(*parser <InfixArrayGet>)
		(*disj 2)
		(*parser <Whitespace>) *star
		(*caten 3)
		(*pack-with (lambda (ws1 expr ws2) expr))
		(*parser (word "**"))
		(*parser (word "^"))
		(*disj 2)
		(*parser <Whitespace>) *star
		(*parser (delayed (lambda () <InfixFuncall>)))
		(*parser <InfixArrayGet>)
		(*disj 2)
		(*caten 3) *star
		(*caten 2)
		(*pack-with (lambda (base expt)
			(if (equal? expt '())
				base 
				(let* ((first-base base) 
					(rest-numbers (map caddr expt))
				 	(entire-lst (append (list first-base) rest-numbers)))
						(build-pow entire-lst))
				)))
	done))


(define mulDiv-builder
	(lambda (lst-nums lst-ops) 
		(if (equal? (length lst-nums) 2)
			(list (car lst-ops) (cadr lst-nums) (car lst-nums))
			(list (car lst-ops) (mulDiv-builder (cdr lst-nums) (cdr lst-ops)) (car lst-nums))
		)
	)
)

(define <InfixDivisionAndMultiplication>
	(new
		(*parser <Whitespace>) *star
		(*parser <InfixPower>)
		(*parser <Whitespace>) *star
		(*caten 3)
		(*parser (word "*"))
		(*parser (word "/"))
		(*disj 2)
		(*parser <Whitespace>) *star
		(*parser <InfixPower>)
		(*caten 3) *star
		(*caten 2) 
		(*pack-with(lambda (expr1 expr2)
			(if (equal? expr2 '())
				(cadr expr1)		
			(let* ((firstNum(cadr expr1)) 
					(restNums (map caddr expr2)) 
				 	(numList (append (list firstNum) restNums))
					(opList (map (lambda (el) (string->symbol(string(caar el)))) expr2)))
					(mulDiv-builder (reverse numList) (reverse opList)) )))) 
		
	done))

(define <InfixSymbol>
	(new
		(*parser <Symbol>)
		(*parser (const (lambda (ch) 
			(or (char=? ch #\+)
			 (char=? ch #\-)
			 (char=? ch #\*)
			 (char=? ch #\^)
			 (char=? ch #\.)))))
		(*parser (word "**"))
		(*disj 2)
		*not-followed-by
	done))


(define equation-builder2
	(lambda (lst-nums lst-ops)
		(if (equal? (length lst-nums) 2) 
			(list (car lst-ops) (cadr lst-nums) (car lst-nums))
			(list (car lst-ops) (equation-builder2 (cdr lst-nums) (cdr lst-ops)) (car lst-nums)))
		))



(define <InfixSubstractionAndAddition>
	(new
		(*parser <Whitespace>) *star
		(*parser <InfixDivisionAndMultiplication>)
		(*parser <Whitespace>) *star
		(*caten 3)
		(*pack-with (lambda (ws1 expr ws2) expr))
		(*parser (word "+"))
		(*parser (word "-"))
		(*disj 2)
		(*pack (lambda (ans) (list (string->symbol (string (car ans))))))
		(*parser <Whitespace>) *star
		(*parser <InfixDivisionAndMultiplication>) 
		(*caten 3) *star
		(*caten 2)
		(*pack-with (lambda (first-parser sec-parser)
			(if (equal? sec-parser '())
				first-parser
				(let* ((first-base first-parser) 
					(rest-numbers (map caddr sec-parser))
				 	(entire-lst (append (list first-base) rest-numbers))
				 	(ops-lst (map caar sec-parser)))
						(equation-builder2 (reverse entire-lst) (reverse ops-lst)))
				)))
	done))


(define <InfixArgList>
	(new
		(*parser <Whitespace>) *star
		(*parser (delayed (lambda () <InfixFuncall>)))
		(*parser <InfixSubstractionAndAddition>)
		(*disj 2)
		(*parser <Whitespace>) *star
		(*caten 3)
		(*pack-with (lambda (ws1 expr ws2) expr))
		(*parser (word ","))
		(*parser <Whitespace>) *star
		(*parser <InfixExpression>)
		(*parser <Whitespace>) *star
		(*caten 4) *star
		(*pack (lambda (ans) (map caddr ans)))
		(*caten 2)
		(*pack-with (lambda (first rest) (append (list first) rest)))
		(*parser <epsilon>)
		(*disj 2)
	done))

(define <InfixFuncall>
	(new
		(*parser <Whitespace>) *star
		(*parser <HighestPriority>)
		(*parser <Whitespace>) *star
		(*parser <Left-Parentheses>)
		(*parser <Whitespace>) *star
		(*parser <InfixArgList>)
		(*parser <Whitespace>) *star
		(*parser <Right-Parentheses>)
		(*caten 8)
		(*pack-with (lambda (ws1 func ws2 lpar ws3 arg-lst ws4 rpar) (append (list func) arg-lst)))
	done))


(define <InfixSexprEscape>
	(new
		(*parser <InfixPrefixExtensionPrefix>)
		(*parser <sexpr>)
		(*caten 2)
		(*pack-with (lambda (prefix sexpr) sexpr))
	done))

(define <InfixExtension>
	(new
		(*parser <InfixPrefixExtensionPrefix>)
		(*parser <InfixExpression>)
		(*caten 2)
		(*pack-with (lambda (prefix sexp) sexp))
	done))
