(load "project/ass/qq.scm")


(define parse
  (lambda (sexp)
    (cond ((number? sexp) (list 'const sexp))
          ((boolean? sexp) (list 'const sexp))
		  ((equal? sexp '()) (list 'const sexp))
          ((string? sexp) (list 'const sexp))
          ((char? sexp) (list 'const sexp))
          ((symbol? sexp) (make-symbol-exp sexp))
		  ((vector? sexp) (list 'const sexp))
          (else (let ((first (car sexp)))
                  (cond ((quote? sexp)
                         (list 'const (cadr sexp)))
                  		((equal? first 'quasiquote)
							(parse (expand-qq (cadr sexp))))
                        ((equal? first 'if)
							(make-if-exp sexp)	)
						((equal? (isReserved? first) #f)
							(make-applic-exp sexp))
						((equal? first 'set!)
							(make-set!-exp sexp))
						((equal? first 'or)
							(make-or-exp sexp))
						((equal? first 'lambda)
							(make-lambda-exp sexp))
						((equal? first 'begin)
							(make-seq-exp sexp))
						((equal? first 'define)
							(make-define-exp sexp))
                        ((equal? first 'let)
                        	(let-macro sexp))
                        ((equal? first 'let*)
                        	(let*-macro sexp))
                        ((equal? first 'letrec)
                        	(letrec-macro sexp))
                        ((equal? first 'and)
                        	(parse (and-macro sexp)))
                        ((equal? first 'cond)
                            (parse (cond-macro sexp)))
                         )))
						 ))) 	
	
	
(define *reserved-words*
	'(and begin cond define do else if lambda
	let let* letrec or quasiquote unquote
	unquote-splicing quote set!))


(define isReserved?
	(lambda (sym)
		(find (lambda (el) (equal? el sym)) *reserved-words*)))

(define make-symbol-exp
	(lambda (sexp)
		(if (isReserved? sexp) 
			(error sexp "unexpected reserved word") 
			(list 'var sexp))))
		
(define make-lambda-exp 
	(lambda (sexp)
		(if (list? (cadr sexp)) 
			(let ((params (cadr sexp))
					(body (cddr sexp)))
					(list 'lambda-simple params (make-seq-exp body)))
	
			(if (not(pair? (cadr sexp)))
				(list 'lambda-opt '() (cadr sexp) (make-seq-exp (cddr sexp)))
			
				(let ((params (sepratingImproperList(cadr sexp)))
					(rest (cdr(last-pair (cadr sexp))))
					(body (cddr sexp)))
					(list 'lambda-opt params rest (make-seq-exp body))
					
				)
			)
	)
	))
;Input: improper list.
;Output: proper list without the last element.
(define sepratingImproperList 
		(lambda(impList)
			(if (not(pair? (cdr impList)))
				(list (car impList))
				(append (list (car impList))  (sepratingImproperList (cdr impList)))
			)
		)
)
	
(define make-if-exp
	(lambda (sexp)
		(let ((test (parse (cadr sexp))) 
			  (then (parse (caddr sexp)))
			  (void_obj (list 'const (void))))
					(if (equal?(length sexp) 3)  
						(list 'if3 test then void_obj)
						(let ((else_ (parse (cadddr sexp))))
							(list 'if3 test then else_))  )	
		)))
		
(define make-applic-exp
	(lambda (sexp)
		(let ((op (parse (car sexp)))
				(rands (map parse (cdr sexp))))
				(list 'applic op rands) )
				))

(define make-set!-exp
	(lambda (sexp) 
		(let ((var (parse (cadr sexp)))
			  (value (parse (caddr sexp))))
				(list 'set var value)	
			)))
			
(define make-or-exp
	(lambda (sexp) 
		(let ((rands (map parse (cdr sexp))))
				(if (null? rands)
					(list 'const #f)
					(if (equal? (length rands) 1)
						(car rands)
						(list 'or rands))
					)
			)))
			
(define make-seq-exp
	(lambda (sexp)
		(let ((flattened-exp (begin-flatten sexp)))
				(cond
					((not(equal? 'begin (car flattened-exp)))		
						(if (equal? (length flattened-exp) 1)
							(parse (car flattened-exp))
							(list 'seq (map parse flattened-exp))))														
					((equal? (length flattened-exp) 2) (parse (cadr flattened-exp)))	
					((equal? (length flattened-exp) 1) (list 'const (void)))
					(else (list 'seq (map parse (cdr flattened-exp))))	
				))))
	
(define make-define-exp
	(lambda (sexp) 
		(let ((second (cadr sexp)))
					(if (pair? second)
						(let ((var (car second))
								(params (cdr second))
								(exps `(begin ,@(cddr sexp))))
									(list 'define (parse var)
										(make-lambda-exp (list 'lambda params exps)) ) ) 

						
						(let ((exps (caddr sexp)))
							(list 'define (parse second) (parse exps)) )
						) )))

(define let-macro
	(lambda (sexp)
		(let* ((bindings (cadr sexp))
				(body (cddr sexp))
				(vars (map car bindings))
				(vals (map cadr bindings)))
					(if (have-dups? vars)
						(error vars "Invalid parameter list" )
						(parse `((lambda ,vars ,@body) ,@vals))
						)
					)
		))

(define let*-macro
	(lambda (sexp)
		(let ((bindings (cadr sexp))
				(body(cddr sexp)))
					(if (equal? bindings '())
						(parse `(let () ,@body))
						(parse (compose-let* bindings body)))
				)
		))

(define compose-let*
	(lambda (bindings body)
		(if (equal? (length bindings) 1)
			`(let ,(list (car bindings)) ,@body)
			(append (list 'let (list (car bindings))) (list (compose-let* (cdr bindings) body)))
			)))


(define letrec-macro
	(lambda (sexp)
		(let* ((bindings (cadr sexp))
				(body (cddr sexp))
				(vars (map car bindings))
				(vals (map cadr bindings)))
					(parse `(let ,(map (lambda (el) (list el #f)) vars) ,@(map (lambda (var val) (list 'set! var val)) vars vals)  ((lambda () ,@body))))
				)
		))


(define and-macro
	(lambda (sexp)
		(let ((tests (cdr sexp)))
			(cond ((null? tests) #t) 
					((equal? (length tests) 1) (car tests)) 
					((equal? (length tests) 2) (list 'if (car tests) (cadr tests) #f)) 
					(else (list 'if (car tests) (and-macro tests) #f)))))) 


(define cond-macro 
    (lambda(sexp)
	    (if (equal? (length sexp) 2) 
	        (cond 
				((equal? 'else (caadr sexp))  `(begin ,@(cdadr sexp)))
				(else (list 'if (caadr sexp) `(begin ,@(cdadr sexp)))))
	        (let ((body (cdr sexp))
	            (then (cdadr sexp))
	            (test (caadr sexp)))
						(list 'if test `(begin ,@then) (cond-macro (cdr sexp)))
	            
	            )
			)
	))

(define have-dups?
	(lambda (lst)
	  	(cond ((null? lst) #f)
	        ((member (car lst) (cdr lst)) #t)
	        (else (have-dups? (cdr lst))))))

(define begin-flatten
	(lambda (sexp)
		(letrec ((flattening-func (lambda (el)
			(if (list? el)
				(if (equal? 'begin (car el))
					(if (equal? (cdr el) '())
						'((begin))
						(fold-right append '() (map flattening-func (cdr el))))
					(list el))
				(list el))
			))) (fold-right append '() (map flattening-func sexp))))
	)

