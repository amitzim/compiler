(define apply
  (lambda (f s)
    ((lambda () (cgen_apply f s)))
  ))


(define remainder
	(lambda(num1 num2)
		(if (> 0 num1)
			(if (> 0 num2)
				(remainderPos (- 0 num1) (- 0 num2) 1)
				(remainderPos (- 0 num1) num2 1))
			(if (> 0 num2)
				(remainderPos num1 (- 0 num2) 0)
				(remainderPos num1 num2 0)
			))))
			

  
(define remainderPos
	(lambda(num1 num2 flag) 
			(if (= num1 0)
				0
				(if (and (> 0 num1) (= flag 1))
					(- 0 (+ num1 num2))
					(if (> 0 num1)
						(+ num1 num2)							
					(remainderPos (- num1 num2) num2 flag)))
				)))				
				
		
		
(define bin-append
  (lambda (l m)
    (if (null? l)
      m
      (cons (car l) (bin-append (cdr l) m)))
    ))

(define append
  (lambda el
    (if (null? el)
      el
      (fold-left bin-append (car el) (cdr el)))
    ))

(define +
  (lambda s
  (if (null? s)
	0
      (letrec ((fold-left (lambda (f acc lst)
                                      (if (null? lst)
                                        acc
                                        (fold-left f
                                               (f acc (car lst))
                                               (cdr lst))))))
                   (fold-left bin_plus 0 s)))))

(define *
  (lambda s
	(if (null? s)
		1
      (letrec ((fold-left (lambda (f acc lst)
                                      (if (null? lst)
                                        acc
                                        (fold-left f
                                               (f acc (car lst))
                                               (cdr lst))))))
                   (fold-left bin_mul 1 s)))))

(define /
  (lambda s
      (letrec ((fold-left (lambda (f acc lst)
                                      (if (null? lst)
                                        acc
                                        (fold-left f
                                               (f acc (car lst))
                                               (cdr lst))))))
                  (if (null? (cdr s))
                    (bin_div 1 (car s))
                    (fold-left bin_div (car s) (cdr s))))))

(define =
  (lambda s
      (letrec ((eq_helper (lambda (lst num acc)
                            (if (null? lst)
                              (and acc #t)
                              (eq_helper (cdr lst) num (and acc (bin_eq num (car lst)))))
      )))
      (eq_helper (cdr s) (car s) #t)))
      )

(define <
  (lambda s
      (letrec ((eq_helper (lambda (lst num acc)
                            (if (null? lst)
                              (and acc #t)
                              (eq_helper (cdr lst) (car lst) (and acc (bin_bigger num (car lst)))))
      )))
      (eq_helper (cdr s) (car s) #t)))
      )

(define >
  (lambda s
      (letrec ((eq_helper (lambda (lst num acc)
                            (if (null? lst)
                              (and acc #t)
                              (eq_helper (cdr lst) (car lst) (and acc (bin_lower num (car lst)))))
      )))
      (eq_helper (cdr s) (car s) #t)))
      )

(define fold-left
    (lambda (func accum lst)
        (if (null? lst)
          accum
          (fold-left func
                 (func accum (car lst))
                 (cdr lst)))))




(define list (lambda x x))
 
(define map2
  (lambda (f s)
      (if (null? s)
          '()
          (let ((x (f (car s))))
              (cons x (map2 f (cdr s)))))))
 
(define map 
	(lambda (f . s)
		(mapping f s)
	)
)
 
(define mapping
	(lambda (f s) 
		(if (null? (car s))
			'()
			(cons (apply f (map2 car s)) 
			(mapping f (map2 cdr s))))
	)
)

 
(define -
  (lambda s
	(if (null? (cdr s)) (bin_minus 0 (car s))
      (letrec ((fold-left (lambda (func accum lst)
                                      (if (null? lst)
                                        accum
                                        (fold-left func
                                               (func accum (car lst))
                                               (cdr lst))))))
                  (fold-left bin_minus (car s) (cdr s))))))