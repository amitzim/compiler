(load "project/ass/sexpr-parser.scm")
(load "project/ass/tag-parser.scm")
(load "project/ass/semantic-analyzer.scm")


(define label-counter -1)
(define inc
	(lambda ()
		(set! label-counter (+ 1 label-counter)) label-counter))

(define file->list
	(lambda (in-file)
	(let ((in-port (open-input-file in-file)))
		(letrec ((run
			(lambda ()
				(let ((ch (read-char in-port)))
					(if (eof-object? ch)
						(begin (close-input-port in-port) '())
						(cons ch (run)))))))
		(run)))))
(define box?
	(lambda (exp) 
		(and (list? exp) (equal? (car exp) 'box))
	))
(define fvar?
	(lambda (exp) 
		(and (list? exp) (equal? (car exp) 'fvar))
	))

(define pvar?
	(lambda (exp) 
		(and (list? exp) (equal? (car exp) 'pvar))
	))

(define bvar?
	(lambda (exp) 
		(and (list? exp) (equal? (car exp) 'bvar))
	))
(define box-get?
	(lambda (exp) 
		(and (list? exp) (equal? (car exp) 'box-get))
	))
	
(define const-table '())

(define set-const-table
	(lambda (lst)
		(set! const-table lst) const-table
		))

(define fvar-table '())

(define set-fvar-table
	(lambda (lst)
		(set! fvar-table lst) fvar-table
		))

(define tc-applic?
	(lambda (exp)
		(if (equal? (car exp) 'tc-applic) #t #f)
		))

(define box-set?
	(lambda (exp)
		(if (equal? (car exp) 'box-set) #t #f)
		))
		
		
(define lambda-simple?
	(lambda (exp)
		(if (equal? (car exp) 'lambda-simple) #t #f)
		))

(define lambda-opt?
	(lambda (exp)
		(if (equal? (car exp) 'lambda-opt) #t #f)
		))

(define list->set
	(lambda (lst)
		(if (null? lst)
			'()
			(if (is-member? (car lst) (cdr lst))
				(list->set (cdr lst))
				(append (list (car lst)) (list->set (cdr lst)))
				)
			)
		))

(define remove-known-literals
	(lambda (lst)
		(remove #f (remove #t (remove '() lst)))
		))

(define collect-fvars
	(lambda (exp acc)
		(cond ((null? exp) acc)
			  ((fvar? exp) (append acc (list (cadr exp))))
			  ((list? exp) (append (collect-fvars (car exp) acc) (collect-fvars (cdr exp) acc)))
			  (else '())
			)
		))

(define fraction?
	(lambda (num)
		(if (and (not (integer? num)) (number? num)) #t #f)
		))


(define sub-consts
	(lambda (lst acc)
		(if (equal? '() lst)
			(append acc (list '()))
			
			(if (not (list? (car lst)))
				(if (pair? (car lst))
					(append (list acc) (sub-consts (list (caar lst)) (car lst)) (sub-consts (list (cdar lst)) (cdar lst)) (sub-consts (cdr lst) (cdr lst)))
					(if (fraction? (car lst))
						(append (list acc) (list (car lst)) (list (numerator(car lst)) (denominator (car lst))) (sub-consts (cdr lst) (cdr lst)))
						(append (list acc) (list (car lst)) (sub-consts (list (cdr lst)) (cdr lst)))))
				(append (list acc) (sub-consts (car lst) (car lst)) (sub-consts (cdr lst) (cdr lst)))
				)
			)
		
		))

(define split-fractions
	(lambda (lst)
		(if (null? lst)
			'()
			(if (fraction? (car lst))
				(append (list (numerator (car lst))) (list (denominator (car lst))) (list (car lst)) (split-fractions (cdr lst)))
				(append (list (car lst)) (split-fractions (cdr lst)))
				))))

(define collect-consts
	(lambda (exp acc)
		(if (empty? exp)
			'()
			(if (constTexp? exp)
					(if (vector? (cadr exp))
						(append acc (cdr exp) (split-fractions (vector->list (cadr exp))))
						(if (list? (cadr exp))
							(append acc (sub-consts (cadr exp) (cadr exp)))
							(if (pair? (cadr exp))
								(append acc (sub-consts (list (cadr exp)) (cadr exp)))
								(if (fraction? (cadr exp))
									(append acc (list (cadr exp)) (list (numerator(cadr exp))) (list (denominator (cadr exp))))
									(append acc (list (cadr exp)))))))
					(if (list? (car exp))
						(append acc (collect-consts (car exp) '()) (collect-consts (cdr exp) '()))
						(append acc (collect-consts (cdr exp) '()))
						)
					))
		))


(define lookup-in-const-table
	(lambda (val table)
		(cond ((null? val) "c_nil")
			((equal? #t val) "c_true")
			((equal? #f val) "c_false")
			((if (empty? table)
								(error "lookup-in-const-table" (format "~a not found in lookup" val))
								(if (equal? (cadar table) val)
									(caar table)
									(lookup-in-const-table val (cdr table))
									)
								)))
		))

(define lookup-in-fvar-table
	(lambda (fvar table)
		(if (empty? table)
			(error "lookup-in-fvar-table" (format "~a not found in lookup" fvar))
			(let ((fvar-from-tbl (cadar table)))
				
				(if (equal? fvar-from-tbl fvar)
						(caar table)
						(lookup-in-const-table fvar (cdr table))
						)
					)		)	
		))

(define type->number
	(lambda (exp)
		(cond 
			((equal? exp (void)) 1)
			((null? exp ) 2)
			((integer? exp) 3)
			((fraction? exp) 4)
			((boolean? exp) 5)
			((char? exp) 6)
			((string? exp) 7)
			((symbol? exp) 8)
			((procedure? exp) 9)
			((pair? exp) 10)
			((vector? exp) 11)
			)
		))


(define build-const-table
	(lambda (consts-lst)
		(letrec ((inc (lambda (counter) (set! counter (+ 1 counter)) counter)) 
			(create-table (lambda (counter inc consts-lst acc) 
				(if (empty? consts-lst)
					acc
					(append acc (list (list (string-append "c" (number->string (inc counter))) (car consts-lst))) (create-table (inc counter) inc (cdr consts-lst) '())))
				)))
			(create-table -1 inc consts-lst '()))
		))


(define build-fvar-table
	(lambda (fvar-lst)
		(letrec ((inc (lambda (counter) (set! counter (+ 1 counter)) counter)) 
			(create-table (lambda (counter inc fvar-lst acc) 
				(if (empty? fvar-lst)
					acc
					(append acc (list (list (string-append "fvar" (number->string (inc counter))) (car fvar-lst))) (create-table (inc counter) inc (cdr fvar-lst) '())))
				)))
			(create-table -1 inc fvar-lst '()))
		))

(define get-label
	(lambda (lit)
		(string-append (lookup-in-const-table lit const-table) ":")
		))

(define get-fvar-label
	(lambda (fvar)
		(string-append (lookup-in-fvar-table fvar fvar-table) ":")
		))

(define vec-sub-labels
	(lambda (lst)
		(if (null? lst)
			""
			(if (null? (cdr lst))
				(string-append (lookup-in-const-table (car lst) const-table))
				(string-append (lookup-in-const-table (car lst) const-table) "," (vec-sub-labels (cdr lst)))
			))
		))

(define get-macro
	(lambda (num-type lit)
	
		(cond 
			((equal? num-type 0) (format "~a\n\tdq SOB_UNDEFINED\n" (get-label lit )))
			((equal? num-type 1) (format "~a\n\tdq SOB_VOID\n" (get-label lit )))
			((equal? num-type 2) (format "~a\n\tdq SOB_NIL\n" (get-label lit )))
			((equal? num-type 3) (format "~a\n\tdq MAKE_LITERAL(T_INTEGER, ~a) \n" (get-label lit) lit ))
			((equal? num-type 4) (format "~a\n\tdq MAKE_FRAC(~a, ~a)\n" (get-label lit) (lookup-in-const-table (numerator lit) const-table) (lookup-in-const-table (denominator lit) const-table)))
			((equal? num-type 5) (if lit (format "~a\n\tdq SOB_TRUE\n" (get-label lit)) (format "~a\n\tdq SOB_FALSE\n" (get-label lit))))
			((equal? num-type 6) (format "~a\n\tdq MAKE_LITERAL(T_CHAR, ~a)\n" (get-label lit ) (char->integer lit)))
			((equal? num-type 7) (format "~a\n\tMAKE_LITERAL_STRING ~s \n" (get-label lit ) lit ))
			((equal? num-type 8) (format "~a\n\tMAKE_LITERAL_SYMBOL ~a\n.string:\n\t MAKE_LITERAL_STRING ~s\n" (get-label lit ) (string-append (lookup-in-const-table lit const-table)".string")(symbol->string lit)))
			((equal? num-type 10)(format "~a\n\tdq MAKE_LITERAL_PAIR(~a, ~a) \n" (get-label lit) (lookup-in-const-table (car lit) const-table) (lookup-in-const-table (cdr lit) const-table) )) 
			((equal? num-type 11) (let ((args (vector->list lit)))
				(if (null? args)
					(format "~a\n\tdq MAKE_LITERAL(T_VECTOR, 0)\n" (get-label lit))
					(format "~a\n\t MAKE_LITERAL_VECTOR ~a \n" (get-label lit) (vec-sub-labels args)))
					))
			)
		)
	)


(define intialize-consts
	(lambda (tab1)
		(if (empty? tab1)
			""
			(let* ((first (car tab1)) (label (car first)) (val (cadr first)) (type (type->number val)))
				(string-append (get-macro type val) (intialize-consts (cdr tab1)))
				)
			)
		))

(define intialize-fvars
	(lambda (tab1)
		(if (empty? tab1)
			""
			(let* ((first (car tab1)) (label (car first)) (val (cadr first)) (type (type->number val)))
				(string-append (string-append label ":\n") (format "\tdq SOB_UNDEFINED \n" ) (intialize-fvars (cdr tab1)))
				)
			)
		))

(define string->file
	(lambda (str target_filename)
		(let ((out-port (open-output-file target_filename)))
			(block-write out-port str)
			(close-output-port out-port) 
			)
	))

(define create-fvar-table
	(lambda (fvar-lst)
		(if (empty? fvar-lst)
			""
			(string-append "" (symbol->string (car fvar-lst)) ":\n\tdq SOB_UNDEFINED\n" (create-fvar-table (cdr fvar-lst)))
			)
		))
(define print-rax
"
	push qword [rax]
	call write_sob_if_not_void
	add rsp, 1*8\n")

(define main
"
section .text
global main
~a

main:
	push rbp
	mov rbp, rsp
~a
	
	~a
	after_init:
~a
	mov rsp, rbp
	pop rbp
	ret

L_exit_error:
	mov rax, 1
	mov rdi, 1
	mov rsi, exitmsg
	mov rdx, 17
	syscall
	mov rax, 60
	mov rdi, 0
	syscall

L_exit_error_out_of_memory:
	mov rax, 1
	mov rdi, 1
	mov rsi, mem_error
	mov rdx, 15
	syscall
	mov rax, 60
	mov rdi, 0
	syscall
"
	)


(define connect_runtime_funcs
	(lambda (fvar-table)	
		(string-append
		(format "\tCONNECT_FUNC ~a, not_code\n" (lookup-in-fvar-table 'not fvar-table))
		(format "\tCONNECT_FUNC ~a, cons_code\n" (lookup-in-fvar-table 'cons fvar-table))
		(format "\tCONNECT_FUNC ~a, car_code\n" (lookup-in-fvar-table 'car fvar-table))
		(format "\tCONNECT_FUNC ~a, cdr_code\n" (lookup-in-fvar-table 'cdr fvar-table))
		(format "\tCONNECT_FUNC ~a, zero?_code\n" (lookup-in-fvar-table 'zero? fvar-table))
		(format "\tCONNECT_FUNC ~a, pair?_code\n" (lookup-in-fvar-table 'pair? fvar-table))
		(format "\tCONNECT_FUNC ~a, null?_code\n" (lookup-in-fvar-table 'null? fvar-table))
		(format "\tCONNECT_FUNC ~a, number?_code\n" (lookup-in-fvar-table 'number? fvar-table))
		(format "\tCONNECT_FUNC ~a, eq?_code\n" (lookup-in-fvar-table 'eq? fvar-table))
		(format "\tCONNECT_FUNC ~a, boolean?_code\n" (lookup-in-fvar-table 'boolean? fvar-table))
		(format "\tCONNECT_FUNC ~a, char?_code\n" (lookup-in-fvar-table 'char? fvar-table))
		(format "\tCONNECT_FUNC ~a, integer?_code\n" (lookup-in-fvar-table 'integer? fvar-table))
		(format "\tCONNECT_FUNC ~a, procedure?_code\n" (lookup-in-fvar-table 'procedure? fvar-table))
		(format "\tCONNECT_FUNC ~a, string?_code\n" (lookup-in-fvar-table 'string? fvar-table))
		(format "\tCONNECT_FUNC ~a, symbol?_code\n" (lookup-in-fvar-table 'symbol? fvar-table))
		(format "\tCONNECT_FUNC ~a, vector?_code\n" (lookup-in-fvar-table 'vector? fvar-table))
		(format "\tCONNECT_FUNC ~a, bin_plus_code\n" (lookup-in-fvar-table 'bin_plus fvar-table))		
		(format "\tCONNECT_FUNC ~a, bin_minus_code\n" (lookup-in-fvar-table 'bin_minus fvar-table))		
		(format "\tCONNECT_FUNC ~a, bin_mul_code\n" (lookup-in-fvar-table 'bin_mul fvar-table))		
		(format "\tCONNECT_FUNC ~a, bin_div_code\n" (lookup-in-fvar-table 'bin_div fvar-table))		
		(format "\tCONNECT_FUNC ~a, bin_eq_code\n" (lookup-in-fvar-table 'bin_eq fvar-table))		
		(format "\tCONNECT_FUNC ~a, bin_bigger_code\n" (lookup-in-fvar-table 'bin_bigger fvar-table))		
		(format "\tCONNECT_FUNC ~a, bin_lower_code\n" (lookup-in-fvar-table 'bin_lower fvar-table))		
		(format "\tCONNECT_FUNC ~a, vector_length_code\n" (lookup-in-fvar-table 'vector-length fvar-table))		
		(format "\tCONNECT_FUNC ~a, string_length_code\n" (lookup-in-fvar-table 'string-length fvar-table))		
		(format "\tCONNECT_FUNC ~a, numerator_code\n" (lookup-in-fvar-table 'numerator fvar-table))		
		(format "\tCONNECT_FUNC ~a, denominator_code\n" (lookup-in-fvar-table 'denominator fvar-table))		
		(format "\tCONNECT_FUNC ~a, cgen_apply_code\n" (lookup-in-fvar-table 'cgen_apply fvar-table))		
		(format "\tCONNECT_FUNC ~a, vector_ref_code\n" (lookup-in-fvar-table 'vector-ref fvar-table))		
		(format "\tCONNECT_FUNC ~a, string_ref_code\n" (lookup-in-fvar-table 'string-ref fvar-table))
		(format "\tCONNECT_FUNC ~a, string_set_code\n" (lookup-in-fvar-table 'string-set! fvar-table))		
		(format "\tCONNECT_FUNC ~a, vector_set_code\n" (lookup-in-fvar-table 'vector-set! fvar-table))
		(format "\tCONNECT_FUNC ~a, vector_code\n" (lookup-in-fvar-table 'vector fvar-table))
		(format "\tCONNECT_FUNC ~a, make_vector_code\n" (lookup-in-fvar-table 'make-vector fvar-table))
		(format "\tCONNECT_FUNC ~a, rational?_code\n" (lookup-in-fvar-table 'rational? fvar-table))
		(format "\tCONNECT_FUNC ~a, integer_to_char_code\n" (lookup-in-fvar-table 'integer->char fvar-table))
		(format "\tCONNECT_FUNC ~a, char_to_integer_code\n" (lookup-in-fvar-table 'char->integer fvar-table))
		(format "\tCONNECT_FUNC ~a, symbol_string_code\n" (lookup-in-fvar-table 'symbol->string fvar-table))
		(format "\tCONNECT_FUNC ~a, string_symbol_code\n" (lookup-in-fvar-table 'string->symbol fvar-table))
		(format "\tCONNECT_FUNC ~a, make_string_code\n" (lookup-in-fvar-table 'make-string fvar-table))
			))
)

(define runtime_support
"
make_string_code:
	push rbp
	mov rbp, rsp

	mov rax, arg_count
	cmp rax, 1
	je .one_arg
	mov rdi, A1 
	mov rdi, [rdi]
	DATA rdi
	jmp .cont
	.one_arg:
		mov rdi, 0
	.cont:
		mov rbx, A0 
		mov rbx, [rbx]
		DATA rbx
		mov rdx, rbx
		CALL_MALLOC rbx
		mov rbx, [ptr]
		push rdx
		push rdi
		push rbx
		call init_string
		pop rbx
		add rsp, 2 * 8

		shl rdx, 30
		sub rbx, start_of_data
		or rdx, rbx
		shl rdx, TYPE_BITS
		or rdx, T_STRING
		CALL_MALLOC 8
		mov rax, [ptr]
		mov [rax], rdx

	.die:
	mov rsp, rbp
	pop rbp
	ret


init_string:
	push rbp
	mov rbp, rsp
	
	mov r8, [rbp + 2 * 8] 
	mov rbx, [rbp + 3 * 8] 
	mov rcx, [rbp + 4 * 8] 
	cmp rcx, 0
	je .die
	mov rax, r8
	.init_loop:
		mov byte [r8], bl
		inc r8
		loop .init_loop

	.die:
	mov rsp, rbp
	pop rbp
	ret

make_vector_code:
	push rbp
	mov rbp, rsp

	mov rax, arg_count
	
	cmp rax, 1
	je .one_arg
	mov rax, A0 
	mov rbx, A1 
	jmp .cont
	.one_arg:
		CALL_MALLOC 8
		mov rbx, [ptr]
		mov qword [rbx], 0x3
		mov rax, A0 
	.cont:
	mov rcx, qword [rax]
	DATA rcx
	mov rdx, rcx
	cmp rdx, 0
	je .next
	.push_loop:
		push rbx
		loop .push_loop
	.next:
	push rdx
	push 0
	call vector_code
	lea rdx, [rdx * 8]
	add rsp, rdx
	add rsp, 2 * 8
	mov rsp, rbp
	pop rbp
	ret

vector_code:
	push rbp
	mov rbp, rsp
	lol:
	mov rbx, arg_count
	mov rcx, rbx
	shl rbx, 3
	CALL_MALLOC rbx
	shr rbx, 3
	mov rax, [ptr]
	shl rbx, ((WORD_SIZE - TYPE_BITS) >> 1)
	mov rsi, 0
.loop:
	cmp rcx, rsi
	je .end
	lea rdx, [rbp + 4 * 8 + 8 * rsi]
	mov rdx, qword [rdx]
	mov [rax], rdx
	inc rsi
	add rax, 8
	jmp .loop
.end:
	mov rax, [ptr]
	sub rax, start_of_data
	or rbx, rax
	shl rbx, TYPE_BITS
	or rbx, T_VECTOR
	CALL_MALLOC 8
	mov rax, [ptr]
	mov [rax], rbx
	mov rsp, rbp
	pop rbp
	ret
rational?_code:	
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov r8, A0 
	mov r8, [r8]
	TYPE r8
	cmp r8,T_INTEGER
	je .true
	cmp r8,T_FRACTION
	je .true
	.false:
		mov rax,c_false
		jmp .die
	.true:
		mov rax, c_true
	.die:
	mov rsp, rbp
	pop rbp
	ret
integer_to_char_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax,1
	jne .die
	mov rax, A0 
	mov rbx,[rax]
	TYPE rbx
	cmp rbx,T_INTEGER
	jne .die
	mov rbx,[rax]
	DATA rbx
	cmp rbx,0
	jl .die
	cmp rbx,256
	jge .die
	mov rbx,[rax]
	xor rbx,(T_CHAR ^ T_INTEGER)
	CALL_MALLOC 8
	mov rax,[ptr]
	mov [rax],rbx
	
	.die:
	mov rsp,rbp
	pop rbp
	ret

char_to_integer_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax,1
	jne .die
	mov rax, A0 
	mov rbx,[rax]
	TYPE rbx
	cmp rbx,T_CHAR
	jne .die
	mov rbx,[rax]
	mov rbx,[rax]
	xor rbx,(T_INTEGER ^ T_CHAR)
	CALL_MALLOC 8
	mov rax,[ptr]
	mov [rax],rbx
	.die:
		mov rsp,rbp
		pop rbp
		ret
			
	

string_set_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 3
	jne .die
	mov r8, A0 	
	mov r8, [r8]
	mov r9,A1		
	mov r9,[r9]
	DATA r9
	mov r10,A2 		
	mov r10,[r10]
	shr r10,4		
	STRING_SET rsi,r8,r9,r10
	mov rax,c_void
	.die:
	mov rsp, rbp
	pop rbp
	ret

vector_set_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 3
	jne .die
	mov r8, A0 	
	mov r8, [r8]
	mov r9,A1		
	mov r9,[r9]
	DATA r9
	mov r10,A2 		
	mov r10,[r10]
	VECTOR_SET rsi,r8,r9,r10
	mov rax,c_void
	.die:
	mov rsp, rbp
	pop rbp
	ret

vector_ref_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 2
	jne .die
	mov rbx, A0 	
	mov rbx, [rbx]
	mov rcx,A1  	
	mov rcx,[rcx]
	DATA rcx
	VECTOR_REF rsi,rbx,rcx
	mov rax,rsi
	.die:
	mov rsp, rbp
	pop rbp
	ret
	
string_ref_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 2
	jne .die
	mov rbx, A0 	
	mov rbx, [rbx]
	mov rcx,A1  	
	mov rcx,[rcx]
	DATA rcx
	xor rsi,rsi
	STRING_REF rsi,rbx,rcx
	shl rsi,TYPE_BITS
	or rsi,T_CHAR
	CALL_MALLOC 8
	mov rax,[ptr]
	mov [rax],rsi
	.die:
	mov rsp, rbp
	pop rbp
	ret

cgen_apply_code:
	push rbp
	mov rbp, rsp
	mov r8, ret_addr
	mov r9, env
	mov r10, A0 
	mov rax, A1 
	add rsp, 8 * 6
	mov r11, rsp
	mov r13, rbp
	mov rbp, old_rbp
	.loop:
		cmp rax, c_nil
		je .end_loop
		mov rsi, [rax]
		mov rdi, [rax]
		MY_CAR rsi
		MY_CDR rdi
		mov rax, rdi
		push rsi
		jmp .loop
	.end_loop: 
	mov r12, rsp
	mov rbx, r11
	sub r11, 8
	mov rcx, r12
	sub rbx, rcx
	shr rbx, 3 
	.copy_loop:
		mov rdx, r12
		sub rdx, r11
		cmp rdx, 8
		jbe .end_copy_loop
		mov r14, [r11]
		mov r15, [r12]
		mov [r11], r15
		mov [r12], r14
		sub r11, 8
		add r12, 8
		jmp .copy_loop
	.end_copy_loop:
	push rbx 
	mov rcx, r10 
	mov rcx, [rcx]
	TYPE rcx 
	cmp rcx, T_CLOSURE
	jne L_exit_error
	mov rbx, r10
	mov rbx, [rbx]
	CLOSURE_ENV rbx
	push rbx	
	push r8		
	mov r10, [r10]
	CLOSURE_CODE r10
	jmp r10


denominator_code:
	push rbp
	mov rbp, rsp
	mov rcx, arg_count
	cmp rcx, 1 
	jne .die
	mov rax, A0
	mov rbx, A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx, T_FRACTION
	je .frac
	CALL_MALLOC 8
	mov rbx, [ptr]
	mov qword [rbx], 0x13
	mov rax, rbx
	jmp .die
.frac:
	mov rax, [rax]
	MY_CDR rax
.die:
	mov rsp, rbp
	pop rbp
	ret

numerator_code:
	push rbp
	mov rbp, rsp
	mov rcx, arg_count
	cmp rcx, 1 
	jne .die
	mov rax, A0
	mov rbx, A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx, T_FRACTION
	jne .die
	mov rax, [rax]
	MY_CAR rax
.die:
	mov rsp, rbp
	pop rbp
	ret

vector_length_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die	
	mov rbx, A0
	mov rbx, [rbx]
	VECTOR_LENGTH rbx
	shl rbx,TYPE_BITS
	or rbx,T_INTEGER
	CALL_MALLOC 8
	mov rax,[ptr]
	mov [rax],rbx
	.die:
	mov rsp, rbp
	pop rbp
	ret	
	
	
string_length_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die	
	mov rbx, A0
	mov rbx, [rbx]
	STRING_LENGTH rbx
	shl rbx, TYPE_BITS
	or rbx, T_INTEGER
	CALL_MALLOC 8
	mov rax,[ptr]
	mov [rax],rbx
	.die:
	mov rsp, rbp
	pop rbp
	ret

bin_lower_code:
	push rbp
	mov rbp, rsp
	mov rax, A0
	mov rax, [rax]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .denom1
	mov rbx, rax
	DATA rbx
	mov rax, 1
	jmp .arg1_int
.denom1:
	mov rbx, rax
	CAR rbx
	DATA rbx
	CDR rax
	DATA rax
.arg1_int:
	mov r10, rax 
	mov r8, rbx 
	mov rbx, A1
	mov rbx, [rbx]
	mov rax, rbx
	TYPE rax
	cmp rax, T_INTEGER
	jne .denom2
	DATA rbx
	mov rcx, rbx
	mov rbx, 1
	jmp .arg2_int
.denom2:
	mov rcx, rbx
	CAR rcx
	DATA rcx
	CDR rbx
	DATA rbx
.arg2_int:
	mov r11, rbx 
	mov r9, rcx 
	push r10
	push r11
	call gcd
	add rsp, 2* 8
	push rax
	push r10
	push r11
	call lcm
	add rsp, 3 * 8
	mov r14, rax 
	xor rdx, rdx
	cqo
	idiv r10
	mov rsi, rax 
	mov rax, r14
	xor rdx, rdx
	cqo
	idiv r11
	mov rdi, rax 
	mov rax, r8
	imul rax, rsi
	mov rsi, rax
	mov rax, r9
	imul rax, rdi
	mov rdi, rax
	cmp rsi, rdi 
	jg .true
	mov rax, c_false
	jmp .end
	.true:
	mov rax, c_true
	.end:
	mov rsp, rbp
	pop rbp
	ret



bin_bigger_code:
	push rbp
	mov rbp, rsp
	mov rax, A0
	mov rax, [rax]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .denom1
	mov rbx, rax
	DATA rbx
	mov rax, 1
	jmp .arg1_int
.denom1:
	mov rbx, rax
	CAR rbx
	DATA rbx
	CDR rax
	DATA rax
.arg1_int:
	mov r10, rax 
	mov r8, rbx 
	mov rbx, A1
	mov rbx, [rbx]
	mov rax, rbx
	TYPE rax
	cmp rax, T_INTEGER
	jne .denom2
	DATA rbx
	mov rcx, rbx
	mov rbx, 1
	jmp .arg2_int
.denom2:
	mov rcx, rbx
	CAR rcx
	DATA rcx
	CDR rbx
	DATA rbx
.arg2_int:
	mov r11, rbx 
	mov r9, rcx 
	push r10
	push r11
	call gcd
	add rsp, 2* 8
	push rax
	push r10
	push r11
	call lcm
	add rsp, 3 * 8
	mov r14, rax 
	xor rdx, rdx
	cqo
	idiv r10
	mov rsi, rax 
	mov rax, r14
	xor rdx, rdx
	cqo
	idiv r11
	mov rdi, rax 
	mov rax, r8
	imul rax, rsi
	mov rsi, rax
	mov rax, r9
	imul rax, rdi
	mov rdi, rax
	cmp rsi, rdi 
	jl .true
	mov rax, c_false
	jmp .end
	.true:
	mov rax, c_true
	.end:
	mov rsp, rbp
	pop rbp
	ret

bin_div_code:
	push rbp
	mov rbp, rsp

	mov rax, A0
	mov rax, [rax]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .denom1
	mov rbx, rax
	DATA rbx
	mov rax, 1
	jmp .arg1_int
.denom1:
	mov rbx, rax
	CAR rbx
	DATA rbx
	CDR rax
	DATA rax
.arg1_int:
	mov r10, rax 
	mov r8, rbx 
	mov rbx, A1
	mov rbx, [rbx]
	mov rax, rbx
	TYPE rax
	cmp rax, T_INTEGER
	jne .denom2
	DATA rbx
	mov rcx, rbx
	mov rbx, 1
	jmp .arg2_int
.denom2:
	mov rcx, rbx
	CAR rcx
	DATA rcx
	CDR rbx
	DATA rbx
.arg2_int:
	mov r11, rbx 
	mov r9, rcx 
	cmp r9, 0
	jl .convert
	jmp .continue
	.convert:
	neg r9
	neg r11
	.continue:
	xchg r9, r11
	imul r9, r8 
	imul r11, r10 
	push r9
	push r11
	call gcd
	pop r11
	pop r9
	mov r12, rax 
	mov rax, r11
	cqo
	idiv r12
	mov r11, rax
	mov rax, r9
	cqo
	idiv r12
	mov r9, rax
	mov rsi, r9
	mov rdi, r11


	shl rsi, TYPE_BITS
	or rsi, T_INTEGER
	shl rdi, TYPE_BITS
	or rdi, T_INTEGER
	CALL_MALLOC 8
	mov rbx, [ptr]
	mov [rbx], rsi
	CALL_MALLOC 8
	mov rcx, [ptr]
	mov [rcx], rdi
	CALL_MALLOC 8
	mov rax, [ptr]
	MAKE_DYNAMIC_FRACTION rax, rbx, rcx
	push rax
	push rbx
	push rcx
	call before_print_frac
	add rsp, 3 * 8 
	mov rsp, rbp
	pop rbp
	ret



bin_eq_code:
	push rbp
	mov rbp, rsp
	cmp arg_count, 1
	je .true
	mov rax, A0
	mov rax, [rax]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .denom1
	mov rbx, rax
	DATA rbx
	mov rax, 1
	jmp .arg1_int
.denom1:
	mov rbx, rax
	CAR rbx
	DATA rbx
	CDR rax
	DATA rax
.arg1_int:
	mov r10, rax 
	mov r8, rbx 
	mov rbx, A1
	mov rbx, [rbx]
	mov rax, rbx
	TYPE rax
	cmp rax, T_INTEGER
	jne .denom2
	DATA rbx
	mov rcx, rbx
	mov rbx, 1
	jmp .arg2_int
.denom2:
	mov rcx, rbx
	CAR rcx
	DATA rcx
	CDR rbx
	DATA rbx
.arg2_int:
	mov r11, rbx 
	mov r9, rcx 
	push r10
	push r11
	call gcd
	add rsp, 2* 8
	push rax
	push r10
	push r11
	call lcm
	add rsp, 3 * 8
	mov r14, rax 
	xor rdx, rdx
	cqo
	idiv r10
	mov rsi, rax 
	mov rax, r14
	xor rdx, rdx
	cqo
	idiv r11
	mov rdi, rax 
	mov rax, r8
	imul rax, rsi
	mov rsi, rax
	mov rax, r9
	imul rax, rdi
	mov rdi, rax
	cmp rsi, rdi 
	je .true
	mov rax, c_false
	jmp .end
	.true:
	mov rax, c_true
	.end:
	mov rsp, rbp
	pop rbp
	ret


bin_mul_code:
	push rbp
	mov rbp, rsp
	mov rax, A0
	mov rax, [rax]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .denom1
	mov rbx, rax
	DATA rbx
	mov rax, 1
	jmp .arg1_int
.denom1:
	mov rbx, rax
	CAR rbx
	DATA rbx
	CDR rax
	DATA rax
.arg1_int:
	mov r10, rax 
	mov r8, rbx 
	mov rbx, A1
	mov rbx, [rbx]
	mov rax, rbx
	TYPE rax
	cmp rax, T_INTEGER
	jne .denom2
	DATA rbx
	mov rcx, rbx
	mov rbx, 1
	jmp .arg2_int
.denom2:
	mov rcx, rbx
	CAR rcx
	DATA rcx
	CDR rbx
	DATA rbx
.arg2_int:
	mov r11, rbx 
	mov r9, rcx 
	imul r9, r8 
	imul r11, r10 
	push r9
	push r11
	call gcd
	pop r11
	pop r9
	mov r12, rax 
	mov rax, r11
	cqo
	idiv r12
	mov r11, rax
	mov rax, r9
	cqo
	idiv r12
	mov r9, rax
	mov rsi, r9
	mov rdi, r11


	shl rsi, TYPE_BITS
	or rsi, T_INTEGER
	shl rdi, TYPE_BITS
	or rdi, T_INTEGER
	CALL_MALLOC 8
	mov rbx, [ptr]
	mov [rbx], rsi
	CALL_MALLOC 8
	mov rcx, [ptr]
	mov [rcx], rdi
	CALL_MALLOC 8
	mov rax, [ptr]
	MAKE_DYNAMIC_FRACTION rax, rbx, rcx
	push rax
	push rbx
	push rcx
	call before_print_frac
	add rsp, 3 * 8 
	mov rsp, rbp
	pop rbp
	ret


bin_plus_code:
	push rbp
	mov rbp, rsp
	mov rax, A0
	mov rax, [rax]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .denom1
	mov rbx, rax
	DATA rbx
	mov rax, 1
	jmp .arg1_int
.denom1:
	mov rbx, rax
	CAR rbx
	DATA rbx
	CDR rax
	DATA rax
.arg1_int:
	mov r10, rax 
	mov r8, rbx 
	mov rbx, A1
	mov rbx, [rbx]
	mov rax, rbx
	TYPE rax
	cmp rax, T_INTEGER
	jne .denom2
	DATA rbx
	mov rcx, rbx
	mov rbx, 1
	jmp .arg2_int
.denom2:
	mov rcx, rbx
	CAR rcx
	DATA rcx
	CDR rbx
	DATA rbx
.arg2_int:
	mov r11, rbx 
	mov r9, rcx 
	push r10
	push r11
	call gcd
	add rsp, 2* 8
	push rax
	push r10
	push r11
	call lcm
	add rsp, 3 * 8
	mov r14, rax 
	xor rdx, rdx
	cqo
	idiv r10
	mov rsi, rax 
	mov rax, r14
	xor rdx, rdx
	cqo
	idiv r11
	mov rdi, rax 
	mov rax, r8
	imul rax, rsi
	mov rsi, rax
	mov rax, r9
	imul rax, rdi
	mov rdi, rax
	add rsi, rdi 
	
	mov r13, rsi 
	push r13
	push r14
	call gcd
	pop r14
	pop r13
	mov r12, rax 
	mov rax, r13
	cqo
	idiv r12
	mov r13, rax
	mov rax, r14
	cqo
	idiv r12
	mov r14, rax
	mov rsi, r13
	mov rdi, r14


	shl rsi, TYPE_BITS
	or rsi, T_INTEGER
	shl rdi, TYPE_BITS
	or rdi, T_INTEGER
	CALL_MALLOC 8
	mov rbx, [ptr]
	mov [rbx], rsi
	CALL_MALLOC 8
	mov rcx, [ptr]
	mov [rcx], rdi
	CALL_MALLOC 8
	mov rax, [ptr]
	MAKE_DYNAMIC_FRACTION rax, rbx, rcx
	push rax
	push rbx
	push rcx
	call before_print_frac
	add rsp, 3 * 8 
	mov rsp, rbp
	pop rbp
	ret

lcm:
	push rbp
	mov rbp, rsp
	mov rax, [rbp + 2 * 8]
	mov rbx, [rbp + 3 * 8]
	imul rax ,rbx
	mov rcx, [rbp + 4 * 8] 
	xor rdx, rdx
	div rcx
	mov rsp, rbp
	pop rbp
	ret

gcd:
	push rbp
	mov rbp, rsp

	xor rdx, rdx
	mov rax, [rbp + 2 * 8] 
	mov rbx, [rbp + 3 * 8] 
	iabs rax
	iabs rbx
	cmp rax, rbx
	jge .loop
	xchg rax, rbx
	
.loop:
	cmp rbx, 0
	je .done
	mov rdx, 0
	div rbx
	mov rax, rbx
	mov rbx, rdx
	jmp .loop

.done:
	mov rsp, rbp
	pop rbp
	ret

bin_minus_code:
	push rbp
	mov rbp, rsp
	cmp arg_count, 0
	mov rax, 3
	je .end
	mov rax, A0
	mov rax, [rax]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .denom1
	mov rbx, rax
	DATA rbx
	mov rax, 1
	jmp .arg1_int
.denom1:
	mov rbx, rax
	CAR rbx
	DATA rbx
	CDR rax
	DATA rax
.arg1_int:
	mov r10, rax 
	mov r8, rbx 
	mov rbx, A1
	mov rbx, [rbx]
	mov rax, rbx
	TYPE rax
	cmp rax, T_INTEGER
	jne .denom2
	DATA rbx
	mov rcx, rbx
	mov rbx, 1
	jmp .arg2_int
.denom2:
	mov rcx, rbx
	CAR rcx
	DATA rcx
	CDR rbx
	DATA rbx
.arg2_int:
	mov r11, rbx 
	mov r9, rcx 
	push r10
	push r11
	call gcd
	add rsp, 2* 8
	push rax
	push r10
	push r11
	call lcm
	add rsp, 3 * 8
	mov r14, rax 
	xor rdx, rdx
	cqo
	idiv r10
	mov rsi, rax 
	mov rax, r14
	xor rdx, rdx
	cqo
	idiv r11
	mov rdi, rax 
	mov rax, r8
	imul rax, rsi
	mov rsi, rax
	mov rax, r9
	imul rax, rdi
	mov rdi, rax
	sub rsi, rdi 
	
	mov r13, rsi 
	push r13
	push r14
	call gcd
	pop r14
	pop r13
	mov r12, rax 
	mov rax, r13
	cqo
	idiv r12
	mov r13, rax
	mov rax, r14
	cqo
	idiv r12
	mov r14, rax
	mov rsi, r13
	mov rdi, r14
	
	shl rsi, TYPE_BITS
	or rsi, T_INTEGER
	shl rdi, TYPE_BITS
	or rdi, T_INTEGER
	CALL_MALLOC 8
	mov rbx, [ptr]
	mov [rbx], rsi 
	CALL_MALLOC 8
	mov rcx, [ptr]
	mov [rcx], rdi 
	CALL_MALLOC 8
	mov rax, [ptr]
	MAKE_DYNAMIC_FRACTION rax, rbx, rcx
	push rax
	push rbx
	push rcx
	call before_print_frac
	add rsp, 3 * 8 
.end:
	mov rsp, rbp
	pop rbp
	ret

before_print_frac:
	push rbp
	mov rbp, rsp
	CALL_MALLOC 8
	push rcx
	push rdx
	mov rcx, [rbp + 2 * 8] 
	mov rcx, [rcx]
	mov rdx, [rbp + 3 * 8] 
	mov rdx, [rdx]
	DATA rcx
	DATA rdx
	cmp rcx, 1
	je .denom_is_1
	cmp rcx, rdx
	jz .is_one
	cmp rdx, 0
	je .is_zero
	mov rax, [rbp + 4 * 8] 
	jmp .end
.is_zero:
	mov rax, 0
	shl rax, TYPE_BITS
	or rax, T_INTEGER
	mov rdx, [ptr]
	mov [rdx], rax
	mov rax, rdx
	jmp .end
.is_one:
	mov rax, 1
	shl rax, TYPE_BITS
	or rax, T_INTEGER
	mov rdx, [ptr]
	mov [rdx], rax
	mov rax, rdx
	jmp .end

.denom_is_1:
	mov rax, [rbp + 3 * 8]
.end:
	pop rdx
	pop rcx
	mov rsp, rbp
	pop rbp
	ret


string?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rax, A0
	mov rbx, [rax]
	TYPE rbx
	cmp rbx, T_STRING
	jne .false
	mov rax, c_true
	jmp .die
	.false:
	mov rax, c_false
	.die:
	pop rbp
	ret


procedure?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rax, A0
	mov rbx, [rax]
	TYPE rbx
	cmp rbx, T_CLOSURE
	jne .false
	mov rax, c_true
	jmp .die
	.false:
	mov rax, c_false
	.die:
	pop rbp
	ret


integer?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rax, A0
	mov rbx, [rax]
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .false
	mov rax, c_true
	jmp .die
	.false:
	mov rax, c_false
	.die:
	pop rbp
	ret


char?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rax, A0
	mov rbx, [rax]
	TYPE rbx
	cmp rbx, T_CHAR
	jne .false
	mov rax, c_true
	jmp .die
	.false:
	mov rax, c_false
	.die:
	pop rbp
	ret

boolean?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rax, A0
	mov rbx, [rax]
	TYPE rbx
	cmp rbx, T_BOOL
	jne .false
	mov rax, c_true
	jmp .die
	.false:
	mov rax, c_false
	.die:
	pop rbp
	ret

eq?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 2
	jne .die
	mov rax, A0
	mov rbx, A1
	cmp rax, rbx
	jne .false
	mov rax, c_true
	jmp .die
	.false:
	mov rax, c_false
	.die:
	pop rbp
	ret

cons_code:
	push rbp
	mov rbp, rsp
	cmp arg_count, 2
	jne .die
	mov rcx, A0
	mov rdx, A1
	CALL_MALLOC 8
	mov rax, [ptr]
	CONS rax, rcx, rdx
	.die:
	pop rbp
	ret

not_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx,A0
	mov rbx, [rbx]
	cmp rbx,SOB_FALSE
	jne .false
	mov rax, c_true
	.die:
	mov rsp, rbp
	pop rbp
	ret
	.false:
	mov rax,c_false
	jmp .die

symbol_string_code:
	push rbp
	mov rbp, rsp

	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx, A0		
	mov rbx, [rbx]	
	mov rax,rbx
	DATA rax
	add rax,start_of_data	
	.die:
	mov rsp, rbp
	pop rbp
	ret
	
string_symbol_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx, A0		
	mov r8,rbx			
	mov rbx, [rbx]
	mov rax,rbx			
	mov r10,[flag]
	cmp r10,0			
	je .init_linked
	mov rbx,[start]
	add rbx,8
	mov rcx, [rbx]
	sub rbx,8
	
	.looping:
	mov rdi,[rbx]			
	mov r10,rdi	  	
	mov rdi,[rdi] 
	
	DATA rdi
	add rdi, start_of_data 
	mov rdi,[rdi]	
	mov r12,rdi
	mov r11,rdi
	STRING_LENGTH r12
	STRING_ELEMENTS r11 
	mov r9,rax
	mov r15,rax
	STRING_LENGTH  r9
	STRING_ELEMENTS r15
	cmp r9,r12
	jne .add_symbol
	.str_loop:
	cmp r12,0
	je .symbol_at_hand
	xor r9,r9
	xor r13,r13
	mov r9b,byte [r11]
	mov r13b,byte [r15]
	cmp r13,r9
	jne .add_symbol
	add r11,1
	add r15,1
	sub r12,1
	jmp .str_loop
	
	
	
	
	cmp rcx,0	
	je .add_symbol
	add rbx,8
	mov rbx,[rbx]
	add rbx,8 
	mov rcx,[rbx]
	sub rbx,8
	jmp .looping
	
	.symbol_at_hand:
		mov rax,r10 
		jmp .die
	.init_linked:
		mov qword[flag],1
		CALL_MALLOC 16
		mov r12,[ptr]
		mov [start],r12
		sub r8,start_of_data
		shl r8,TYPE_BITS
		or r8 ,T_SYMBOL		
		CALL_MALLOC 8
		mov rax,[ptr]
		mov [rax],r8
		
		mov [r12],rax	
		add r12,8
		
		mov qword[r12],0    
		sub r12,8	
		mov [curr],r12 	
		jmp .die
	.add_symbol:
		CALL_MALLOC 16
		mov r12,[ptr]
		sub r8,start_of_data
		shl r8,TYPE_BITS
		or r8 ,T_SYMBOL		
		CALL_MALLOC 8
		mov rax,[ptr]
		mov [rax],r8
		
		mov [r12],rax	
		add r12,8
		
		mov qword[r12],0     
		sub r12,8
		mov r11,[curr]
		add r11,8
		
		mov [r11],r12
		sub r11,8		
		mov [curr],r12 	
	.die:
		mov rsp, rbp
		pop rbp
		ret

number?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx, A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx,T_INTEGER
	je .true
	cmp rbx,T_FRACTION
	je .true
	mov rax,c_false
	.die:
		mov rsp, rbp
		pop rbp
		ret
	.true:
		mov rax,c_true
		jmp .die

symbol?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx, A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx,T_SYMBOL
	je .true
	mov rax, c_false
	.die:
	mov rsp, rbp
	pop rbp
	ret
	.true:
	mov rax, c_true
	jmp .die
	
vector?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx, A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx,T_VECTOR
	je .true
	mov rax, c_false
	.die:
	mov rsp, rbp
	pop rbp
	ret
	.true:
	mov rax, c_true
	jmp .die
	
null?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx, A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx,T_NIL
	jne .false
	mov rax, c_true
	.die:
	mov rsp, rbp
	pop rbp
	ret
	.false:
	mov rax, c_false
	jmp .die

pair?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx,A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx,T_PAIR
	jne .false
	mov rax, c_true
	.die:
	mov rsp, rbp
	pop rbp
	ret
	.false:
	mov rax, c_false
	jmp .die

zero?_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx,A0
	mov rbx, [rbx]
	mov rcx, rbx
	TYPE rcx
	cmp rcx, T_INTEGER
	jne .arg_is_frac
	DATA rbx
	cmp rbx, 0
	jne .false
	.true:
	mov rax, c_true
	.die:
	mov rsp, rbp
	pop rbp
	ret
	.false:
	mov rax, c_false
	jmp .die
	.arg_is_frac:
	CAR rbx
	cmp rbx, 0
	je .true
	jmp .false	



car_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx, A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx, T_PAIR
	jne .die
	mov rax, A0
	mov rax, [rax]
	MY_CAR rax
	.die:
	mov rsp, rbp
	pop rbp
	ret

cdr_code:
	push rbp
	mov rbp, rsp
	mov rax, arg_count
	cmp rax, 1
	jne .die
	mov rbx, A0
	mov rbx, [rbx]
	TYPE rbx
	cmp rbx, T_PAIR
	jne .die
	mov rax, A0
	mov rax, [rax]
	MY_CDR rax
	.die:
	mov rsp, rbp
	pop rbp
	ret

"
)

(define closure-ptrs
	"mem_error:\n\tdq \"Out of memory\",0xa,0\nexitmsg:\n\tdq \"**program exit**\", 10\ndebug:\n\tdq \"**in lambda body!**\", 10\nclosure_ptr:\n\tdq 0 \nstart:\n\tdq 0\ncurr:\n\tdq 0\nflag:\n\tdq 0 \n major_ptr:\n\tdq 0\nargs_array:\n\tdq 0 \nptr:\n\tdq 0 \nc_void:\n\tdq SOB_VOID\nc_nil:\n\tdq SOB_NIL\nc_true:\n\tdq SOB_TRUE\nc_false:\n\tdq SOB_FALSE\n")

(define compile-scheme-file
	(lambda (src_filename target_filename)
		(let* ((pe-list (pipeline (file->list src_filename)))
				(runtime_code (pipeline (file->list "project/runtime-support.scm"))) 
				(fvars-list (list->set (append (list 'make-string 'make-vector 'char->integer 'vector 'vector-set! 'rational? 'integer->char 'string->symbol 'symbol->string 'string-set! 'vector-ref 'string-ref 'bin_eq 'bin_lower 'bin_bigger 'bin_mul 'bin_div 'cgen_apply 'denominator 'numerator 'string-length 'vector-length 'list 'car 'cdr 'cons 'map 'zero? 'pair? 'null? 'eq? 'boolean? 'char? 'integer? 'vector? 'procedure? 'symbol? 'number? 'string? 'not 'bin_plus 'bin_minus) (collect-fvars pe-list '()) (collect-fvars runtime_code '()))))
				(fvar-tble-new (set-fvar-table (build-fvar-table fvars-list)))
				(fvar-lbls (intialize-fvars fvar-tble-new))
				(consts (reverse (list->set (remove-known-literals (append (collect-consts pe-list '()) (collect-consts runtime_code '()))))))
				
				(loc-const-table (set-const-table (build-const-table consts)))
				(consts-lbls (intialize-consts const-table))

				
				
				(data-section (string-append "%include \"project/scheme.s\"\nsection .data\n" closure-ptrs consts-lbls fvar-lbls))
				
				
				(text-section (format main runtime_support (connect_runtime_funcs fvar-table) (code-gen-iter runtime_code) (code-gen-iter pe-list)))
				)									
			
			
			(string->file (string-append data-section text-section) target_filename)
			
			)
		))

(define pipeline
	(lambda (s)
		((star <sexpr>) s
			(lambda (m r) (map (lambda (e)
							(annotate-tc
								(pe->lex-pe
									(box-set
										(remove-applic-lambda-nil
											(parse e))))))
							m))
			(lambda (f) 'fail))))


(define const-cgen
	(lambda (val)
		(let* ((label (lookup-in-const-table val const-table))
				(ret (format "mov rax, ~a \n" label )) )
			ret
			)

		))

(define fvar-cgen
	(lambda (val)
		(let* ((label (lookup-in-fvar-table val fvar-table))
				(ret (format "mov rax, [~a] \n" label )) )
			ret
			)

		))

(define seq-cgen
	(lambda (exp-lst depth)
		(if (null? exp-lst)
			""
			(string-append (code-gen (list (car exp-lst)) depth) (seq-cgen (cdr exp-lst) depth))
			)
		))


(define if-cgen
	(lambda (if-exp depth)
		(let* ((test (cadr if-exp)) (dit (caddr if-exp)) (dif (cadddr if-exp))
		 (num (number->string (inc)))	
		 (label-dif (format "L_if3_else_~a" num))
		 (label-exit (format "L_if3_exit_~a" num)))
			(format 
"\n
 
	~a
	cmp rax, c_false
	je ~a
	~a
	jmp ~a
	~a:
	~a
	~a:
	 
"
(code-gen (list test) depth)
label-dif
(code-gen (list dit) depth)
label-exit
label-dif
(code-gen (list dif) depth)
label-exit
)
			)
		))

(define def-cgen
	(lambda (def-exp depth)
		(let* ((var (cadr def-exp)) (val (caddr def-exp)) (real-var (cadr var)) (real-val (cadr val)) (fvar-label (lookup-in-fvar-table real-var fvar-table)))
			(begin 
				
				(format 
"
	~a
	mov [~a], rax 
	mov rax, c_void
	
"
			(code-gen (list val) depth)
			fvar-label
			
			))
			)
		))

(define or-cgen
	(lambda (or-body num depth)
		(if (null? or-body)
			(format "or_exit_~a:" num)
			(string-append (code-gen (list (car or-body)) depth) (format "cmp rax, c_false\njne ~a\n" (format "or_exit_~a" num)) (or-cgen (cdr or-body) num depth))
			)
		))

(define cgen-push-args
	(lambda (args depth)
		(apply string-append
			(map (lambda (arg) (string-append (code-gen (list arg) depth) "\tpush rax\n")) (reverse args)))
		))

(define tc-applic-cgen
	(lambda (tc-applic-exp depth num)
		(let* ((op (cadr tc-applic-exp))
				(args (caddr tc-applic-exp)) 
				(push-args-code (cgen-push-args args depth))
				(num-args (length args)))
				(begin 
					
					(string-append "\n\tpush c_nil\n\t" push-args-code (format "\tpush ~a\n" num-args) (code-gen (list op) depth) 
				(format 
"
	mov rcx, rax 
	mov rcx, [rcx]
	TYPE rcx 
	cmp rcx, T_CLOSURE
	jne L_exit_error
	mov rbx, rax
	mov rbx, [rbx]
	CLOSURE_ENV rbx
	push rbx	
		
	mov rcx ,ret_addr
	push rcx


	mov rcx, arg_count 
	add rcx, 3
	shl rcx, 3 
	
	mov rbx, rbp
	mov rbp, old_rbp
	
	mov rdx, [rsp + 2 * 8]  
	add rdx, 2 
	mov rdi, rdx 
	add rdi, 1 
	shl rdx, 3
	

	copy_frame_~a:
		cmp rdi, 0   
		je .cont
		mov rsi, qword [rsp + rdx] 
		mov qword [rbx + rcx], rsi 
		sub rcx, 8
		sub rdx, 8
		sub rdi, 1
		jmp copy_frame_~a
	.cont:
	
	mov rax, [rax]
	CLOSURE_CODE rax
	
	lea rsp,[rbx + rcx + 8]
	jmp rax 
	
	
	"
		num num)))
			)
		))


(define applic-cgen
	(lambda (applic-exp depth num)
		(let* ((op (cadr applic-exp))
				(args (caddr applic-exp)) 
				(push-args-code (cgen-push-args args depth))
				(num-args (length args)))
				(begin 
					
					(string-append "\n\tpush c_nil\n\t" push-args-code (format "\tpush ~a\n" num-args) (code-gen (list op) depth) 
				(format 
"
	mov rcx, rax 
	mov rcx, [rcx]
	TYPE rcx 
	cmp rcx, T_CLOSURE
	jne L_exit_error
	mov rbx, rax
	mov rbx, [rbx]
	CLOSURE_ENV rbx
	push rbx	
	mov rax, [rax]
	CLOSURE_CODE rax
	call rax 
	

	add rsp, 1*8 
	pop rcx 
	lea rcx, [8*rcx]
	add rsp, rcx
	add rsp, 1 * 8
	
	"
		)))
			)
		))


(define lambda-code-depth-not-zero
	(lambda (depth num)
		(format 
"
	mov rdi, 8 * ~a 
	add rdi, 8	
	CALL_MALLOC rdi
	mov rax, [ptr]
	mov [major_ptr], rax 
	mov rdi, arg_count 
	shl rdi, 3 
	CALL_MALLOC rdi
	mov rax, [ptr]
	mov [args_array], rax
	mov rcx, [args_array]
	mov rbx, [major_ptr] 
	mov rdi, 0	
	mov rdx, rcx 
	load_params_to_args_array_~a:
		cmp rdi, arg_count 
		je exit_loop_1_~a
		shl rdi, 3 
		mov rsi, [rbp + 4 * 8 + rdi] 
		shr rdi, 3 
		mov [rdx], rsi
		add rdx, 8 
		inc rdi
		jmp load_params_to_args_array_~a
	exit_loop_1_~a:
		mov rdi, 1
		mov [rbx], rcx 
		mov rax, [rbp + 2 * 8] 
		cmp rax, 0	
		je exit_loop_2_~a
		mov rsi, [rax] 
	copy_vals_of_major_~a:
		cmp rdi, ~a + 1
		je exit_loop_2_~a
		add rbx, 8
		mov qword [rbx], rsi
		add rax, 8
		mov rsi, qword [rax]
		inc rdi
		jmp copy_vals_of_major_~a
	exit_loop_2_~a:

"
depth num num num num num num depth num num num)

		))

(define lambda-rest-of-code
	(lambda (depth num body fix-stack-code)
		(format
"
	
		mov rdi, 2*8
		CALL_MALLOC rdi
		mov rax, [ptr]
		mov rsi, body_lambda_~a
		mov rdx, rax
		mov rbx, [major_ptr]
		MAKE_LITERAL_CLOSURE rdx, rbx, rsi
		jmp exit_lambda_~a
	body_lambda_~a:
		push rbp
		mov rbp, rsp
		
		
		~a
				
		~a
		
		mov rsp, rbp
		pop rbp
		ret
	exit_lambda_~a:
	

"
num num num  fix-stack-code  (code-gen (list body) (+ 1 depth))  num
	)
		))

(define fix-stack-code
	(lambda (num-params num)
		(begin 
			
			(format 
"
	mov r10, arg_count
	add r10, 1
	cmp r10, ~a
	je end_lst_~a.end

	mov r9, arg_count
	cmp r9, 0
	je end_lst_~a.end
	fix_num_~a:
	mov rax, arg_count
	sub rax, 1
	shl rax, 3
	mov rbx, rbp
	add rbx, 4 * 8
	add rbx, rax 
	shr rax, 3
	mov rcx, c_nil
	mov rdi, ~a
	sub rdi, 1
	sub rax, rdi
	add rax, 1
	create_list_~a:
		cmp rax, 0
		je end_lst_~a
		CALL_MALLOC 8
		mov rsi, [ptr]
		mov rdx, [rbx]
		CONS rsi, rdx, rcx 
		sub rbx, 8
		sub rax, 1
		mov rcx, rsi
		jmp create_list_~a
	end_lst_~a:
	
	mov qword [rbp + 4 * 8 + (~a - 1) * 8], rcx
	

	.end:


		"
			num-params num num num num-params num num num num num-params ))
		))

(define cgen-lambda-simple
	(lambda (lambda-simple-exp depth type)
		(let ((params (cadr lambda-simple-exp)) (num (inc)))
			
			(if (equal? type 'simple)
				(let ((body (caddr lambda-simple-exp)))
					(if (= depth 0)
						(string-append "\tmov rbx, 0\n" (lambda-rest-of-code depth num body ""))
						(string-append (lambda-code-depth-not-zero depth num) (lambda-rest-of-code depth num body ""))
						)	)
				(let* ((body (cadddr lambda-simple-exp)) (num-params (+ 1 (length params))) (fix-stack (fix-stack-code num-params num))) 
					(if (= depth 0)
						(string-append "\tmov rbx, 0\n" (lambda-rest-of-code depth num body fix-stack))
						(string-append (lambda-code-depth-not-zero depth num) (lambda-rest-of-code depth num body fix-stack))
						)
					)
				)
			)
		))

(define pvar-cgen
	(lambda (pvar-exp)
		(let ((minor (caddr pvar-exp)))
			(format "\tmov rax, qword [rbp + 4 * 8 + ~a * 8] \n" minor)
			)
		))

(define bvar-cgen
	(lambda (bvar-exp)
		(let ((major (caddr bvar-exp)) (minor (cadddr bvar-exp)))
			
(string-append (format 
"
	
	mov rax, qword [rbp + 2 * 8] 
	mov rax, qword [rax + 8 * ~a]	
	mov rax, qword [rax + 8 * ~a]	

	
"
			major minor) )
			)
		))


(define code-gen-iter
	(lambda (pe)
		(if (null? pe)
			""
			(string-append (code-gen (list (car pe)) 0) print-rax (code-gen-iter (cdr pe)))
			)	
	))
(define symbol-cgen
	(lambda(symbol inc)
	(let ((label (lookup-in-const-table symbol const-table)))
						
	(format 
	"
	mov rax, ~a 
	
	CALL_MALLOC 16
	mov r8,[ptr]
	mov [r8],rax	
	mov r12,r8
	add r12,8
	mov qword[r12],0
	
	mov r10,[flag]
	cmp r10,0
	je .first_time_~a
	mov rcx,[curr]
	add rcx,8
	mov [rcx],r8	
	mov [curr],r8
	.first_time_~a:
		mov qword[flag],1
		mov [start],r8	
		mov [curr],r8	
	mov rax,~a
	
	"
	
	label inc inc label)
	
	)
))
(define code-gen
	(lambda (pe depth)
		(begin 
			
			(cond 
				
				((and(constTexp? (car pe))(symbol? (cadar pe))) (symbol-cgen (cadar pe) (inc)))
				((constTexp? (car pe)) (const-cgen (cadar pe)))
				((seq? (car pe)) (seq-cgen (cadar pe) depth))
				((if? (car pe)) (if-cgen (car pe) depth))
				((or? (car pe)) (or-cgen (cadar pe) (inc) depth))
				((or (and (set? (car pe)) (fvar? (cadar pe))) (define? (car pe))) (def-cgen (car pe) depth))
				((bvar? (car pe)) (bvar-cgen (car pe)))
				((pvar? (car pe)) (pvar-cgen (car pe)))
				((fvar? (car pe)) (fvar-cgen (cadar pe)))
				((lambda-simple? (car pe)) (begin (cgen-lambda-simple (car pe) depth 'simple)))
				((lambda-opt? (car pe)) (begin  (cgen-lambda-simple (car pe) depth 'opt)))
				((applic? (car pe)) (applic-cgen (car pe) depth (inc)))
				((tc-applic? (car pe)) (tc-applic-cgen (car pe) depth (inc)))
				((box-get? (car pe)) (box-get-cgen (car pe) depth (inc)))
				((box-set? (car pe)) (box-set-cgen (car pe) depth (inc)))
				((box? (car pe)) (box-cgen (car pe) depth (inc)))
				((and(set? (car pe))(pvar? (cadr(car pe)))) (set-pvar-cgen (car pe) depth (inc)))
				
						))
			)
	)
	
(define box-get-cgen
	(lambda(exp depth inc)
	(let ((var (cadr exp)))
	(format
	"
	~a
	mov rax,qword[rax]
	
	"
	(code-gen (list var) depth) ))
	))
	
(define box-cgen
	(lambda(exp depth inc)
	(let ((value (cadr exp))) 
	(format
	"
	 ~a
	 CALL_MALLOC 8
	 mov rbx,[ptr]
	 mov [rbx],rax
	 mov rax,rbx

	
	"
	(code-gen (list value) depth)))
))	

(define box-set-cgen
	(lambda(exp depth inc)
	(let ((value (caddr exp))(var (cadr exp))) 
	(format
	"
	 ~a
	 mov rbx,rax
	 ~a
	 mov [rax],rbx
	 mov rax,c_void
	
	"
	(code-gen (list value) depth)(code-gen (list var) depth))
)))	
	
	
(define set-pvar-cgen
	(lambda(exp depth inc)
	(let ((value (caddr exp))(minor (car(cddadr exp)))) 
	
	(format
	"
	 ~a
	 mov [rbp + 8*(~a + 4)],rax
	 mov rax,c_void
	"
	(code-gen (list value) depth) minor)
)))
(let ((input-filename (cadr (command-line)))
	  (output-filename (caddr (command-line))))
	  (compile-scheme-file input-filename output-filename))